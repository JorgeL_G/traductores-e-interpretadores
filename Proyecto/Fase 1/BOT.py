#!/usr/bin/python
"""
TRADUCTORES E INTERPRETADORES (CI-3725)
Main File

Profesor Ricardo Monascal

Autores:
Jorge Luis Leon       09-11133
Maria Esther Carrillo 10-10122
"""
import sys
from LexBot import *


def main():

    # Construimos el Lexer
    if (len(sys.argv) > 1):
        m.lexicographical_analyzer(sys.argv[1])
    else:
        print 'ERROR: Verifique que el archivo este escrito corectamente'
        print './LexBot <nombre del archivo>\n'


if __name__ == '__main__':
    main()

# Bot.python
