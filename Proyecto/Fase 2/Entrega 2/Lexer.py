#!/usr/bin/env python

################################################################################
#                 ANALIZADOR LEXICOGRAFICO DEL LENGUAJE TRINITY                #
################################################################################ 

import ply.lex as lexer
import sys

# Lista de los errores encontrados en el analizador lexicografico.
lista_errores = []

# Creamos un diccionario con las palabras reservadas para el lenguaje.
reservadas = {
	'program' : 'tkprogram',
	'read' : 'tkread',
	'false' : 'tkfalse',
	'true' : 'tktrue',
	'boolean' : 'tkboolean',
	'number' : 'tknumber',
	'matrix' : 'tkmatrix',
	'row' : 'tkrow',
	'col' : 'tkcol',
	'not' : 'tknot',
	'div' : 'tkdiv',
	'mod' : 'tkmod',
	'print' : 'tkprint',
	'use' : 'tkuse',
	'in' : 'tkin',
	'end' : 'tkend',
	'while' : 'tkwhile',
	'set' : 'tkset',
	'if' : 'tkif',
	'then' : 'tkthen',
	'else' : 'tkelse',
	'for' : 'tkfor',
	'do' : 'tkdo',
	'function' : 'tkfunction',
	'begin' : 'tkbegin',
	'return' : 'tkreturn'
}

# Creamos una lista que almacena todos los tipos de tokens del lenguaje y 
# al final agregamos el diccionario de palabras reservadas para conformar 
# los tokens del lenguaje.
tokens = [
		'punto_coma',
		'coma',
		'dos_puntos',
		'comillas_simples', 
		'mayor',
		'mayor_igual',
		'menor',
		'menor_igual',
		'parentesis_abre',
		'parentesis_cierra',
		'llave_abre',
		'llave_cierra',
		'corchete_abre',
		'corchete_cierra',
		'conjuncion',
		'disyuncion',
		'equivalencia',
		'distinto',
		'mas',
		'menos',
		'por',
		'igual',
		'division_exacta',
		'resto_exacto',
		'numero',
		'identificador',
		'salto_linea',
		'comentarios',
		'cadenas_caracteres',
		'mas_punto',
		'menos_punto',
		'por_punto',
		'division_exacta_punto',
		'resto_exacto_punto',
		'div_punto',
		'mod_punto',
	] + list(reservadas.values())


# Definimos las expresiones regulares para cada uno de los tokens.

t_punto_coma = r'\;'
t_coma = r'\,'
t_dos_puntos = r'\:'
t_comillas_simples = r'\'' 
t_mayor = r'\>'
t_mayor_igual = r'\>='
t_menor = r'\<'
t_menor_igual = r'\<='
t_parentesis_abre = r'\('
t_parentesis_cierra = r'\)'
t_llave_abre = r'\{'
t_llave_cierra = r'\}'
t_corchete_abre = r'\['
t_corchete_cierra = r'\]'
t_conjuncion = r'\&'
t_disyuncion = r'\|'
t_equivalencia = r'\=='
t_distinto = r'\/='
t_mas = r'\+'
t_menos = r'\-'
t_por = r'\*'
t_igual = r'\='
t_division_exacta = r'\/'
t_resto_exacto = r'\%'
t_mas_punto = r'\.\+\.'
t_menos_punto = r'\.\-\.'
t_por_punto = r'\.\*\.'
t_division_exacta_punto = r'\.\/\.'
t_resto_exacto_punto = r'\.\%\.'
t_div_punto = r'\.div\.' 
t_mod_punto = r'\.mod\.'
t_ignore = ' \t' # Ignora espacios y tabulaciones


#
# Funcion que se encarga una vez que se encuentre un numero convertirlo en 
# entero.
#
def t_numero(t):
	r'[-+]?[0-9]*\.?[0-9]+'
	t.value = float(t.value)
	return t

#
# Funcion que maneja los identificadores.
#
def t_identificador(t):
	r'[a-zA-Z][a-zA-Z_0-9]*'
	t.type = reservadas.get(t.value,'identificador') 
	return t

#
# Funcion que se encarga de los saltos de lineas.
#
def t_salto_linea(t):
	r'\n+'
	t.lexer.lineno += len(t.value)

#
# Funcion que se encarga de manejar los comentarios. NO se agregan en la 
# lista de tokens.
#
def t_comentarios(t):
	r'\#[^\n]*'
	t.type = reservadas.get(t.value,'comentarios')
	pass

#
# Funcion que maneja una cadena de caracteres.
#
def t_cadenas_caracteres(t):
	r'\"([^\\\n]|(\\.))*?\"'
	valor = t.value[1:len(t.value)-1]
	t.value = valor.replace('\\"','"')
	t.type = reservadas.get(t.value,'cadenas_caracteres')
	return t

#
# Funcion que se encarga de los caracteres inesperados que no se encuentran 
# en el lenguaje.
#
def t_error(t):
	lista_errores.append(t)
	t.lexer.skip(1)

#
# Funcion que calcula el numero de columna en el que se encuentra un token.
#
def columna_token(input,token):
	ultimo = input.rfind('\n',0,token.lexpos)
	if (ultimo < 0):
		ultimo = 1
		columna = 1
	else:
		columna = (token.lexpos - ultimo)
	return columna


#
# Funcion que simula el analizador lexicografico del lenguaje Trinity.
#
def analizador_lexicografico(archivo_texto,flag):
	# Lista en la que se guardan los token encontrados en el archivo de texto.
	tokens_encontrados = []	

	try:
		
		# Abrimos el archivo de texto
		archivo = open(archivo_texto)

		# Leemos el archivo de texto
		data = archivo.read()

		# Construimos el lexer
		l = lexer.lex()
		l.input(data)

		# Vamos obteniendo los tokens y los guardaremos en una lista
		while True:
		    tok = lexer.token()
		    if not tok: break
		    # Se agrego el token a la lista 'tokens_encontrados'
		    tokens_encontrados.append(tok)

		# Cerramos el archivo de texto
		archivo.close()

		return tokens_encontrados

	except IOError:

		print 'ERROR: No se pudo abrir el archivo de texto \"%s\"' % archivo_texto
		exit()

		# Verificamos si se encontro algun error, en caso de ser asi, se imprime 
	# en pantalla. En caso contrario, se imprime los tokens encontrados.
	if ((len(lista_errores) == 0) & flag):

		for i in tokens_encontrados:
			if (i.type == 'identificador') or (i.type == 'cadenas_caracteres'):
				s1 = "(Linea: %d, Columna: %d) %s: %s"
				print s1 % (i.lineno,columna_token(data,i),i.type,i.value)
			elif (i.type == 'numero'):
				s2 = "(Linea: %d, Columna: %d) %s: %d"
				print s2 % (i.lineno,columna_token(data,i),i.type,i.value)
			else:
				s3 = "(Linea: %d, Columna: %d) %s"
				print s3 % (i.lineno,columna_token(data,i),i.type)

		# Termina con un estado de salida igual a cero porque no se 
		# encontraron errores
		return 0

	else:

		for error in lista_errores:
			e = "ERROR: Caracter inesperado \"%s\" (Linea: %d, Columna: %d)"
			print e % (error.value[0],error.lineno,columna_token(data,error))

		# Termina con un estado de salida distinto de cero porque se 
		# encontraron errores			
		return 1

# END Lexer.py
