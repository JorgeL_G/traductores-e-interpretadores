#
# Modulo que une todas las clases que seran usadas en el archivo Parser.py
#

import sys

simbolos_binarios = {	
						"+" 	: "Suma",
						"-" 	: "Resta",
						"*" 	: "Multiplicacion",
						"/" 	: "Division exacta",
						"%" 	: "Resto exacto",
						"div"	: "Division entera",
						"mod"	: "Resto entero",
						">" 	: "Mayor",
						">=" 	: "Mayor o igual",
						"<" 	: "Menor",
						"<=" 	: "Menor o igual",
						"==" 	: "Equivalencia",
						"\/=" 	: "Distinto",
						"&" 	: "Conjuncion",
						"|" 	: "Disyuncion",
						".+." 	: "Suma cruzada",
						".-." 	: "Resta cruzada",
						".*." 	: "Multiplicacion cruzada",
						"./." 	: "Division exacta cruzada",
						".%." 	: "Resto exacto cruzado",
						".div."	: "Division entera cruzada",
						".mod."	: "Resto entero cruzado",
					}

#
# Funcion que se encarga de la espaciacion adecuada para la impresion
#
def espaciacion(espacio):
	i = 0
	while (i < 4):
		espacio += " "
		i += 1
	return espacio


################################################################################
#                     CLASES PARA EL ANALIZADOR SINTACTICO                     #
################################################################################ 

#
# Clase Identificador
#
class Identificador:

	def __init__(self,nombre):		
		self.nombre = nombre

	def imprimir(self,espacio):
		print espacio,"Identificador:"
		print espacio,"  Nombre:",self.nombre

#
# Clase Literal Numerico
#
class Literal_Numerico:

	def __init__(self,valor):
		self.valor = valor

	def imprimir(self,espacio):
		print espacio,"Literal numerico:"
		print espacio,"  Valor:",self.valor

#
# Clase Cadena de Caracteres
#
class Cadena_Caracteres:

	def __init__(self,valor):
		self.valor = valor
	
	def imprimir(self,espacio):
		print espacio,"Cadena de Caracteres:"
		print espacio,"  Valor:",self.valor		
		
#
# Clase Booleano
#
class Booleano:

	def __init__(self,valor):
		self.valor = valor

	def imprimir(self,espacio):
		print espacio,"Booleano:"
		print espacio,"  Valor:",self.valor

#
# Clase Matriz 
# matrix(n, m)
#
class Matrix:

	def __init__(self,r,c):
		self.r = r
		self.c = c		

	def imprimir(self,espacio):
		print espacio,"Tipo Matrix:"
		print espacio,"  Numero de filas:"
		self.r.imprimir(espaciacion(espacio))
		print espacio,"  Numero de columnas:"
		self.c.imprimir(espaciacion(espacio))

#
# Clase Vector Col
# col(r) = matrix(r,1)
#
class Vector_Col:

	def __init__(self,r):
		self.r = r

	def imprimir(self,espacio):
		print espacio,"Tipo Vector Col:"
		print espacio,"  Numero de filas:"
		self.r.imprimir(espaciacion(espacio))

#
# Clase Vector Row
# row(c) = matrix(1,c)
#
class Vector_Row:

	def __init__(self,c):
		self.c = c

	def imprimir(self,espacio):
		print espacio,"Tipo Vector Row:"
		print espacio,"  Numero de columnas:"
		self.c.imprimir(espaciacion(espacio))

#
# Clase Vector
# e[i, j] o e[i]
#
class Vector:

	def __init__(self,identificador,i,j=None):
		self.identificador = identificador
		self.i = i
		self.j = j

	def imprimir(self,espacio):
		print espacio,"Vector:"
		print espacio,"  Expresion:"
		self.identificador.imprimir(espaciacion(espacio))
		print espacio,"  Posicion i:"
		self.i.imprimir(espaciacion(espacio))
		if self.j:
			print espacio,"  Posicion j:"
			self.j.imprimir(espaciacion(espacio))

#
# Clase Matriz
# {<valores>,<valores>, ...}
# {<valores>:<valores>: ...}
# {<valores>,<valores>:<valores>,<valores>}
#
class Matriz:
	def __init__(self,valor):
		self.valor = valor
		
	def imprimir(self,espacio):
		print espacio,"Matriz:"
		print espacio,"  Valor:"
		for i in self.valor:
			i.imprimir(espaciacion(espacio))
	
#
# Clase Traspuesta
# e op (traspuesta)
#
class Traspuesta:
	def __init__(self,expresion,operador):		
		self.expresion = expresion
		self.operador = operador

	def imprimir(self,espacio):
		print espacio,"Operador \" ' \":"
		print espacio,"  Expresion:"
   		self.expresion.imprimir(espaciacion(espacio))

#
# Clase Declaracion de Variable
# t i
# t i = e (con inicializacion)
#
class Declaracion_Variable:

	def __init__(self,tipo,identificador,expresion=None):		
		self.tipo = tipo
		self.identificador = identificador		
		self.expresion = expresion

	def imprimir(self,espacio):
		print espacio,"Declaracion de Variable:"
		if (isinstance(self.tipo,int) or isinstance(self.tipo,str)):
			print espacio,"  Tipo:",self.tipo
		else:
			print espacio,"  Tipo:"
			self.tipo.imprimir(espaciacion(espacio))
		self.identificador.imprimir(espaciacion(espacio))			
 		if self.expresion:
			self.expresion.imprimir(espaciacion(espacio))

#
# Clase Asignacion
# set identificador = expresion
#
class Asignacion:

	def __init__(self,identificador,expresion):
		self.identificador = identificador
		self.expresion = expresion

	def imprimir(self,espacio):
		print espacio,"Asignacion:"
		self.identificador.imprimir(espaciacion(espacio))
		self.expresion.imprimir(espaciacion(espacio))

#
# Clase Not
# op e (not)
#
class Expresion_Not:

	def __init__(self,expresion):
		self.expresion = expresion

	def imprimir(self,espacio):
		print espacio,"Operador \"not\":"
   		self.expresion.imprimir(espaciacion(espacio))

#
# Clase Expresion Binaria
# e1 op e2
#
class Expresion_Binaria:

	def __init__(self,expresion1,operador,expresion2):
		self.expresion1 = expresion1
		self.expresion2 = expresion2
		self.operador  = operador

	def imprimir(self,espacio):
		s = simbolos_binarios[self.operador]
		print espacio,"Operador:","'"+str(self.operador)+"'",s
		print espacio,"  Operando izquierdo:"
		self.expresion1.imprimir(espaciacion(espacio))
		print espacio,"  Operando derecho:"
		self.expresion2.imprimir(espaciacion(espacio))

#
# Clase Read
# read e;
#
class Read:

	def __init__(self,identificador):
		self.identificador = identificador
		
	def imprimir(self,espacio):
		print espacio,"Read:"
   		self.identificador.imprimir(espaciacion(espacio))

#
# Clase Print
# print e;
#
class Print:

	def __init__(self,expresion):
		self.expresion = expresion

	def imprimir(self,espacio):
		print espacio,"Print:"	
		for i in self.expresion:
			i.imprimir(espaciacion(espacio))

#
# Clase If
# if <condicion> then <instrucciones> end;
#
class Condicional_If:

	def __init__(self,condicion,instruccion):
		self.condicion = condicion
		self.instruccion = instruccion		

	def imprimir(self,espacio):
		print espacio,"Condicional If:"
		print espacio,"  Condicion:"
		self.condicion.imprimir(espaciacion(espacio))
		print espacio,"  Instruccion:"
		for i in self.instruccion:
			i.imprimir(espaciacion(espacio))

#
# Clase If-Else
# if <condicion> then <instrucciones> else <instrucciones> end;
#
class Condicional_If_Else:

	def __init__(self,condicion,instruccion1,instruccion2):
		self.condicion = condicion
		self.instruccion1 = instruccion1
		self.instruccion2 = instruccion2

	def imprimir(self,espacio):
		print espacio,"Condicional If-Else:"
		print espacio,"  Condicion:"
		self.condicion.imprimir(espaciacion(espacio))
		print espacio,"  Intruccion 1:"
		for i in self.instruccion1:
			i.imprimir(espaciacion(espacio))
		print espacio,"  Intruccion 2:"
		for i in self.instruccion2:
			i.imprimir(espaciacion(espacio))

#
# Clase For
# for <identificador> in <vector/matrix> do <instrucciones> end;
#
class Ciclo_For:
	def __init__(self,identificador,vector_matriz,instruccion):
		self.identificador = identificador
		self.vector_matriz = vector_matriz
		self.instruccion = instruccion

	def imprimir(self,espacio):
		print espacio,"Ciclo For:"
		self.identificador.imprimir(espaciacion(espacio))
		self.vector_matriz.imprimir(espaciacion(espacio))
		print espacio,"  Instruccion:"
		for i in self.instruccion:
			i.imprimir(espaciacion(espacio))

#
# Clase While
# while <condicion> do <instrucciones> end;
#
class Ciclo_While:

	def __init__(self,condicion,instruccion):
		self.condicion = condicion
		self.instruccion = instruccion

	def imprimir(self,espacio):
		print espacio,"Ciclo While:"
		print espacio,"  Condicion:"
		self.condicion.imprimir(espaciacion(espacio))
		print espacio,"  Instruccion:"
		for i in self.instruccion:
			i.imprimir(espaciacion(espacio))

#
# Clase Use In End
# use <declaraciones> in <instrucciones> end;
#
class Use_In_End:

	def __init__(self,declaraciones,instruccion):
		self.declaraciones = declaraciones
		self.instruccion = instruccion

	def imprimir(self,espacio):
		print espacio,"Bloque Use In End:"
		print espacio,"Inicio bloque Use:"
		for i in self.declaraciones:
			i.imprimir(espaciacion(espacio))
		print espacio,"Bloque In:"
		for i in self.instruccion:
			i.imprimir(espaciacion(espacio))
		print espacio,"Fin bloque Use In End"

#
# Clase Funcion
# function <identificador>(<parametros>) return <tipo> 
# begin <instrucciones> return <expresion> end;
#
class Funcion:

	def __init__(self,identificador,parametros,tipo,expresion,instruccion=None):
		self.identificador = identificador
		self.parametros = parametros
		self.tipo = tipo
		self.instruccion = instruccion
		self.expresion = expresion

	def imprimir(self,espacio):
		print espacio,"Function:"

		self.identificador.imprimir(espaciacion(espacio))

		print espacio,"  Parametros:"
		for i in self.parametros:			
			i.imprimir(espaciacion(espacio))

		if (isinstance(self.tipo,int) or isinstance(self.tipo,str)):
			print espacio,"  Tipo:",self.tipo
		else:
			print espacio,"  Tipo:"
			self.tipo.imprimir(espaciacion(espacio))

		print espacio,"  Instruccion:"
		if self.instruccion:
			for i in self.instruccion:
				i.imprimir(espaciacion(espacio))

		self.expresion.imprimir(espaciacion(espacio))

		print espacio,"Fin bloque Function End"

#
# Clase Llamada a Funcion
# <identificador>(<parametros>),
#
class Llamada_Funcion:

	def __init__(self,identificador,parametros):
		self.identificador = identificador
		self.parametros = parametros

	def imprimir(self,espacio):
		print espacio,"Llamada a Function:"
		self.identificador.imprimir(espaciacion(espacio))
		print espacio,"  Parametros:"
		for i in self.parametros:			
			i.imprimir(espaciacion(espacio))

#
# Clase Program End
# <funciones> program <bloque> end;
#
class Program_End:

	def __init__(self,bloque,funciones=None):
		self.funciones = funciones
		self.bloque = bloque
		
		# Imprime el programa
		self.imprimir("")
		
	def imprimir(self,espacio):
		print espacio,"Bloque Program End:"
		if self.funciones:
			for i in self.funciones:
				i.imprimir(espaciacion(espacio))
		print espacio,"Bloque Program:"
		print espacio,"  Instrucciones:"
		self.bloque.imprimir(espaciacion(espacio))
		print espacio,"Fin bloque Program End"

# END Clases.py
