#!/usr/bin/python

################################################################################
#                               LENGUAJE TRINITY                               #
#                                                                              #
# Integrantes:                                                                 #
# 10-10231 Paolangela Espinal                                                  #
# 10-11247 Rosangelis Garcia                                                   #
#                                                                              #
################################################################################ 

import sys
import Lexer
import Parser


def main():

	if (len(sys.argv) > 1):
		Lexer.analizador_lexicografico(sys.argv[1],False)
		Parser.analizador_sintactico(sys.argv[1])
	else:
		print "Verifique que la sintaxis introducida es correcta."
		print ".\\trinity <nombre del archivo>\n"


if __name__ == "__main__":
	main()


# END trinity.py
