#!/usr/bin/env python

################################################################################
#				  ANALIZADOR SINTACTICO DEL LENGUAJE TRINITY				  #
################################################################################ 

import sys
import Clases
import Lexer as lexer
import ply.yacc as yacc


# Variable que contendra el archivo a analizar
archivo = ""

#
# Funcion Program End
# <funciones> program <instrucciones> end;
#
def p_Program_End(p):
	'''programa : funcion tkprogram bloque tkend punto_coma
				| tkprogram bloque tkend punto_coma'''
	if len(p) == 6:
		p[0] = Clases.Program_End(p[3],p[1])
	else:
		p[0] = Clases.Program_End(p[2])

#
# Funcion identificador
#
def p_Identificadores(p):
	'''id : identificador'''
	p[0] = Clases.Identificador(p[1])

#
# Funcion literal numerico
#
def p_Literal_Numerico(p):
	'''num : numero'''
	p[0] = Clases.Literal_Numerico(p[1])

#
# Funcion Cadena de Caracteres
#
def p_Cadena_Caracteres(p):
	'''cc : cadenas_caracteres'''
	p[0] = Clases.Cadena_Caracteres(p[1])

#
# Funcion Declaracion de Variable
# t i;
# t i = e; (con inicializacion)
#
def p_Declaracion_Variable(p):
	'''declaracion : declaracion tipo id punto_coma
				   | declaracion tipo id igual expresion punto_coma'''
	p[0] = p[1]
	if len(p) == 5:
		p[0].append(Clases.Declaracion_Variable(p[2],p[3]))
	if len(p) == 7:
		p[0].append(Clases.Declaracion_Variable(p[2],p[3],p[5]))

# Funcion auxiliar base para Declaracion de Variable
def p_Declaracion_Base(p):
	'''declaracion : tipo id punto_coma
			  	   | tipo id igual expresion punto_coma'''
	p[0] = []
	if len(p) == 4:
		p[0].append(Clases.Declaracion_Variable(p[1],p[2]))
	if len(p) == 6:
		p[0].append(Clases.Declaracion_Variable(p[1],p[2],p[4]))

#
# Funcion Asignacion
# set identificador = expresion
#
def p_Asignacion(p):
	'''asignacion : tkset id igual expresion punto_coma
				  | tkset vector igual expresion punto_coma'''
	p[0] = Clases.Asignacion(p[2],p[4])

#
# Funcion booleano
#
def p_Booleano(p):
	'''bool : tktrue
			| tkfalse'''
	p[0] = Clases.Booleano(p[1])

#
# Funcion not
# not bool
#
def p_Expr_Not(p):
	'''expresion : tknot bool'''
	p[0] = Clases.Expresion_Not(p[2])

#
# Funcion con los posibles valores de una expresion
#
def p_Valor_Expresion(p):
	'''expresion : id
				 | num
				 | bool
				 | vector
				 | matrizfc
				 | mtraspuesta'''
	p[0] = p[1]

#
# Funcion con los operadores binarios
# e1 op e2
#
def p_Expr_Binaria(p):
	'''expresion : expresion mas expresion
				 | expresion menos expresion
				 | expresion por expresion
				 | expresion division_exacta expresion
				 | expresion resto_exacto expresion
				 | expresion tkdiv expresion
				 | expresion tkmod expresion
				 | expresion mas_punto expresion
				 | expresion menos_punto expresion
				 | expresion por_punto expresion
				 | expresion division_exacta_punto expresion
				 | expresion resto_exacto_punto expresion
				 | expresion div_punto expresion
				 | expresion mod_punto expresion 
				 | expresion conjuncion expresion
				 | expresion disyuncion expresion
				 | expresion equivalencia expresion
				 | expresion distinto expresion
				 | expresion mayor expresion
				 | expresion mayor_igual expresion
				 | expresion menor expresion
				 | expresion menor_igual expresion'''
	p[0] = Clases.Expresion_Binaria(p[1],p[2],p[3])

#
# Funcion read
# read identificador;
#
def p_Read(p):
	'''lectura : tkread id punto_coma'''
	p[0] = Clases.Read(p[2])

#
# Funcion print
# print <cadena caracteres>
# print <identificador>
# print <cadena caracteres>,<identificador>
#
def p_print(p):
	'''escritura : tkprint argCadena punto_coma'''
	p[0] = Clases.Print(p[2])

# Funcion auxiliar que contiene la lista de los argumentos del print
def p_ArgCadenasCaracteres_Lista(p):
	'''argCadena : argCadena coma caracteres'''
	p[0] = p[1]
	p[0].append(p[3])

# Funcion auxiliar que contiene el argumento base del print
def p_ArgCadenasCaracteres_Base(p):
	'''argCadena : caracteres'''
	p[0] = []
	p[0].append(p[1])

# Funcion auxiliar que contiene los tipos de argumentos del print
def p_ArgCadenasCaracteres(p):
	'''caracteres : cc
				  | expresion'''
	p[0] = p[1]

#
# Funcion Instruccion que contiene la lista de instrucciones
#
def p_Intruccion_Lista(p):
	'''instruccion : instruccion instr_base'''
	p[0] = p[1]
	p[0].append(p[2])

# Funcion auxiliar que contiene una instruccion
def p_Instruccion_Base(p):
	'''instruccion : instr_base'''
	p[0] = []
	p[0].append(p[1])

# Funcion auxiliar que contiene los posibles tipos de una instruccion
def p_Intruccion(p):
	'''instr_base : asignacion
				  | lectura
				  | escritura
				  | condicional
				  | condicional_else
				  | for
				  | while
				  | llamada_func
				  | bloque'''
	p[0] = p[1]

#
# Funcion if 
# if <condicion> then <instrucciones> end;
#
def p_Condicional_If(p):
	'''condicional : tkif expresion tkthen instruccion tkend punto_coma'''
	p[0] = Clases.Condicional_If(p[2],p[4])

#
# Funcion if_else
# if <condicion> then <instrucciones> else <instrucciones> end;
#
def p_Condicional_If_Else(p):
	'''condicional_else : tkif expresion tkthen instruccion tkelse instruccion tkend punto_coma'''
	p[0] = Clases.Condicional_If_Else(p[2],p[4],p[6])

#
# Funcion matriz
# {<valores>,<valores>, ...}
# {<valores>:<valores>: ...}
# {<valores>,<valores>:<valores>,<valores>}
#
def p_Matriz(p):
 	'''matrizfc : llave_abre argm llave_cierra'''
 	p[0] = Clases.Matriz(p[2])


# Funcion auxiliar que contiene un elemento de la fila
def p_ArgFila(p):
	'''argf : num'''
	p[0] = []
	p[0].append(p[1])


# Funcion auxiliar que contiene la lista de los elemento de la fila
def p_ArgFila_Lista(p):
 	'''argf : argf coma num'''
	p[0] = p[1]
	p[0].append(p[3])

# Funcion auxiliar base de la matriz
def p_ArgMat(p):
 	'''argm : argf''' 	
	p[0] = p[1]

# Funcion auxiliar de la lista de los elementos de la matriz
def p_ArgMat_Lista(p):
 	'''argm : argm dos_puntos argf'''
 	p[0] = p[1]
 	for i in p[3]:
 		p[0].append(i)

#
# Funcion que simula la traspuesta de la matriz
# { 1, 2 : 3, 4 }'
#
def p_Matriz_Traspuesta(p):
	'''mtraspuesta : matrizfc comillas_simples'''
	p[0] = Clases.Traspuesta(p[1],p[2])

#
# Funcion For
# for <identificador> in <vector/matrix> do <instrucciones> end;
#
def p_Ciclo_For(p):
	'''for : tkfor id tkin matrizfc tkdo instruccion tkend punto_coma
		   | tkfor id tkin id tkdo instruccion tkend punto_coma'''
	p[0] = Clases.Ciclo_For(p[2],p[4],p[6])

#
# Funcion While
# while <condicion> do <instrucciones> end;
#
def p_Ciclo_While(p):
	'''while : tkwhile expresion tkdo instruccion tkend punto_coma'''
	p[0] = Clases.Ciclo_While(p[2],p[4])

#
# Funcion Use In End
# use <declaraciones> in <instrucciones> end;
#
def p_Use_In_End(p):
	'''bloque : tkuse declaracion tkin instruccion tkend punto_coma'''
	p[0] = Clases.Use_In_End(p[2],p[4])

#
# Funcion con los tipos del lenguaje
#
def p_Tipo(p):
	'''tipo : tknumber
			| tkboolean
			| matrix
			| columna
			| fila'''
	p[0] = p[1]

#
# Funcion Funcion
# function <identificador>(<parametros>) return <tipo> 
# begin <instrucciones> return <expresion> end;
#
def p_Funcion(p):
	'''funcion : funcion tkfunction id parentesis_abre argum parentesis_cierra tkreturn tipo tkbegin instruccion tkreturn expresion punto_coma tkend punto_coma
			   | funcion tkfunction id parentesis_abre argum parentesis_cierra tkreturn tipo tkbegin tkreturn expresion punto_coma tkend punto_coma'''
	p[0] = p[1]
	if len(p) == 16:
		p[0].append(Clases.Funcion(p[3],p[5],p[8],p[12],p[10]))
	if len(p) == 15:
		p[0].append(Clases.Funcion(p[3],p[5],p[8],p[11]))


# Funcion auxiliar que contiene una funcion base
def p_Funcion_Base(p):
	'''funcion : tkfunction id parentesis_abre argum parentesis_cierra tkreturn tipo tkbegin instruccion tkreturn expresion punto_coma tkend punto_coma
			   | tkfunction id parentesis_abre argum parentesis_cierra tkreturn tipo tkbegin tkreturn expresion punto_coma tkend punto_coma'''
	p[0] = []
	if len(p) == 15:
		p[0].append(Clases.Funcion(p[2],p[4],p[7],p[11],p[9]))
	if len(p) == 14:
		p[0].append(Clases.Funcion(p[2],p[4],p[7],p[10]))

# Funcion auxiliar que contiene la lista de los argumentos de una funcion
def p_Argumentos_Lista(p):
	'''argum : argum coma tipo id'''
	p[0] = p[1]
	p[0].append(Clases.Declaracion_Variable(p[3],p[4]))

# Funcion auxiliar que contiene el argumento base de una funcion
def p_Argumentos_Base(p):
	'''argum : tipo id'''
	p[0] = []
	p[0].append(Clases.Declaracion_Variable(p[1],p[2]))

#
# Funcion Llamada a Funcion
# <identificador>(<parametros>)
#
def p_Llamada_Funcion(p):
	'''llamada_func : id parentesis_abre arg parentesis_cierra'''
	p[0] = Clases.Llamada_Funcion(p[1],p[3])

# Funcion auxiliar que contiene la lista de los argumentos de una llamada a 
# funcion
def p_ArgLlamada_Lista(p):
	'''arg : arg coma arg_base'''
	p[0] = p[1]
	p[0].append(p[3])

# Funcion auxiliar que contiene el argumento base de una llamada a funcion
def p_ArgLlamada_Base(p):
	'''arg : arg_base'''
	p[0] = []
	p[0].append(p[1])

# Funcion auxiliar que contiene el tipo de argumento de una llamada a funcion
def p_ArgLlamada(p):
	'''arg_base : expresion'''
	p[0] = p[1]

#
# Funcion matrix
# matrix(n, m)
#
def p_Literal_Matrix(p):
	'''matrix : tkmatrix parentesis_abre num coma num parentesis_cierra
			  | tkmatrix parentesis_abre id coma id parentesis_cierra
			  | tkmatrix parentesis_abre num coma id parentesis_cierra
			  | tkmatrix parentesis_abre id coma num parentesis_cierra'''
	p[0] = Clases.Matrix(p[3],p[5])

#
# Funcion Vector Col
# col(r) = matrix(r,1)
#
def p_Vector_Col(p):
	'''columna : tkcol parentesis_abre num parentesis_cierra
			   | tkcol parentesis_abre id parentesis_cierra'''
	p[0] = Clases.Vector_Col(p[3])

#
# Funcion Vector Row
# row(c) = matrix(1,c)
#
def p_Vector_Row(p):
	'''fila : tkrow parentesis_abre num parentesis_cierra
			| tkrow parentesis_abre id parentesis_cierra'''
	p[0] = Clases.Vector_Row(p[3])

#
# Funcion Vector
# e[i, j] o e[i]
#
def p_vector(p):
	'''vector : id corchete_abre num corchete_cierra
			  | id corchete_abre id corchete_cierra
			  | id corchete_abre num coma num corchete_cierra
			  | id corchete_abre id coma id corchete_cierra
			  | id corchete_abre num coma id corchete_cierra
			  | id corchete_abre id coma num corchete_cierra
			  | matrizfc corchete_abre num corchete_cierra
			  | matrizfc corchete_abre id corchete_cierra
			  | matrizfc corchete_abre num coma num corchete_cierra
			  | matrizfc corchete_abre id coma id corchete_cierra
			  | matrizfc corchete_abre id coma num corchete_cierra
			  | matrizfc corchete_abre num coma id corchete_cierra
			  | mtraspuesta corchete_abre num corchete_cierra
			  | mtraspuesta corchete_abre id corchete_cierra
			  | mtraspuesta corchete_abre num coma num corchete_cierra
			  | mtraspuesta corchete_abre id coma id corchete_cierra
			  | mtraspuesta corchete_abre num coma id corchete_cierra
			  | mtraspuesta corchete_abre id coma num corchete_cierra'''
	if len(p) == 5:
		p[0] = Clases.Vector(p[1],p[3])
	else:
		p[0] = Clases.Vector(p[1],p[3],p[5])

#
# Funcion que agrupa por parentesis
# (e)
#
def p_Expr_Parentesis(p):
	'''expresion : parentesis_abre expresion parentesis_cierra'''
	p[0] = p[2]

#
# Funcion de error
#
def p_error(p):
	if p is not None:	
		e = "Error de sintaxis \"%s\" (Linea: %d, Columna: %d)"
		print e % (p.value,p.lineno,lexer.columna_token(archivo,p))
		exit()		
	else:
	    print "Error de sintaxis: falta de token lo que impide cumplimiento de gramatica"
	    exit()

#
# Lista de precedencia en los operandos
#
precedence = (
	('left','por','division_exacta','resto_exacto','tkdiv','tkmod'),
	('left','mas','menos'),
	('left','por_punto','division_exacta_punto','resto_exacto_punto','div_punto','mod_punto'),
	('left','mas_punto','menos_punto'),
	('left','tknot'),
	('nonassoc','equivalencia','distinto','mayor','mayor_igual','menor','menor_igual'),	
	('left','conjuncion'),
	('left','disyuncion'),
)

#
# Funcion que simula el analizador sintactico del lenguaje Trinity.
#
def analizador_sintactico(archivo_texto):

	try:

		tokens = lexer.tokens
		
		# Abrimos el archivo de texto
		f = open(archivo_texto)

		# Leemos el archivo de texto
		data = f.read()

		global archivo
		archivo = data

		parser = yacc.yacc()
		result = parser.parse(data)

		# Cerramos el archivo de texto
		f.close()

	except IOError:

		print 'ERROR: No se pudo abrir el archivo de texto \"%s\"' % archivo_texto
		exit()

# END Parser.py
