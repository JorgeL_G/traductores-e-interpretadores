"""
Modulo en donde se encuentran todas las clases que seran
utilizadas en Analizador Sintactico (Parser.py)
"""

import sys

simbolos_unarios = {
    "~": "Negacion",
    "-": "Negativo",
}


simbolos_binarios = {
    "+": "Suma",
    "-": "Resta",
    "*": "Multiplicacion",
    "/": "Division",
    "%": "Modulo",
    "/\\": "Conjuncion",
    "\/": "Disyuncion",
    "<": "Menor",
    "<=": "Menor o igual",
    ">": "Mayor",
    ">=": "Mayor o igual",
    "/=": "Distinto",
    "=": "Igual",
    }


"""
Funcion que se encarga de la espaciacion adecuada para la impresion
"""


def espaciacion(espacio):
    i = 0
    while (i < 4):
        espacio += " "
        i += 1
    return espacio

"""
Clase que tendra el Arbol de Expresiones para un solo elemento
"""


class Arbol_Expresion(object):

    def __init__(self, expresion):
        self.expresion = expresion

    def imprimir(self, espacio):
        print espacio, "Expresion"
        print espacio, "    Valor=", self.expresion

"""
Clase que tendra el Arbol para las expresiones unarias
"""


class Arbol_Unario(Arbol_Expresion):

    def __init__(self, operador, expresion):
        self.operador = operador
        self.expresion = expresion

    def imprimir(self, espacio):

        operador_unario = simbolos_unarios[self.operador]
        if self.expresion is not None:
            print espacio, "Operador:", "'"+str(self.operador)+"'", operador_unario
            print espacio, "   Expresion"
            self.expresion.imprimir(espaciacion(espacio))

"""
Clase que tendra el Arbol para expresiones binarias
"""


class Arbol_Binario(Arbol_Expresion):

    def __init__(self, expresion1, operador, expresion2):
        self.expresion1 = expresion1
        self.expresion2 = expresion2
        self.operador = operador

    def imprimir(self, espacio):
        operador_binario = simbolos_binarios[self.operador]
        print espacio, "Operador:", "'"+str(self.operador)+"'", operador_binario
        if self.expresion1 is not None:
            print espacio, "  Operando izquierdo:"
            self.expresion1.imprimir(espaciacion(espacio))
            print espacio, "  Operando derecho:"
        if self.expresion2 is not None:
            self.expresion2.imprimir(espaciacion(espacio))


"""
Clase que todos los arboles van a heredar ya que las hojas
seran palabras reservadas, instrucciones o variables
"""


class Arbol_Instruccion(object):

    def __init__(self, instruccion):
        self.instruccion = instruccion

    def imprimir(self, espacio):
        print self.instruccion


"""
Clase que nos define el arbol para el programa principal
"""


class Arbol_Programa(Arbol_Instruccion):

    def __init__(self, lista_declaraciones, execute):
        self.execute = execute
        self.Arbol_Instruccion = Arbol_Instruccion('create')

        if not(lista_declaraciones is None):
            self.lista_declaraciones = lista_declaraciones
        else:
            self.lista_declaraciones = None

    def imprimir(self, espacio):
        if not(self.execute is None):
            self.Arbol_Instruccion.imprimir(espaciacion(espacio))
            self.lista_declaraciones.imprimir(espaciacion(espacio))
        else:
            self.Arbol_Instruccion.imprimir(espaciacion(espacio))
            self.lista_declaraciones.imprimir(espaciacion(espacio))


"""
Clase que nos define el arbol para las lista de declaraciones de los robots
"""


class Arbol_Lista_Declaraciones(Arbol_Instruccion):

    def __init__(self, declaracion, lista_declaraciones):
        self.declaracion = declaracion
        if not(lista_declaraciones is None):
            self.lista_declaraciones = lista_declaraciones
        else:
            self.lista_declaraciones = None

    def imprimir(self, espacio):
        if not(self.lista_declaraciones is None):
            self.declaracion.imprimir(espacio)
            self.lista_declaraciones.imprimir(espacio)
        else:
            self.declaracion.imprimir(espacio)


"""
Clase que nos define el arbol para la definicion de los robots
"""


class Arbol_Bot(Arbol_Instruccion):

    def __init__(self, tipo, lista_identificadores, lista_comportamientos):
        self.tipo = tipo
        self.arbol_instruccion = Arbol_Instruccion('bot')
        self.lista_identificadores = lista_identificadores
        if (not(lista_comportamientos is None)):
            self.lista_comportamientos = lista_comportamientos
        else:
            self.lista_comportamientos = None

    def imprimir(self, espacio):
        self.tipo.imprimir(espaciacion(espacio))
        self.Arbol_Instruccion.imprimir(espaciacion(espacio))
        self.lista_identificadores.imprimir(espaciacion(espacio))
        if (not(self.lista_comportamientos is None)):
            self.lista_comportamientos.imprimir(espaciacion(espaciacion))


"""
Clase que nos define el arbol para el tipo de los robots
"""


class Arbol_Tipo(Arbol_Instruccion):

    def __init__(self, tipo):
        self.tipo = Arbol_Instruccion(tipo)

    def imprimir(self, espacio):
        self.tipo.imprimir(espaciacion(espacio))


"""
Clase que nos define el arbol para la lista de identificadores
"""


class Arbol_Lista_Identificadores(Arbol_Instruccion):

    def __init__(self, identificador, lista):
        self.identificador = identificador

        if not(lista is None):
            self.lista = lista
        else:
            self.lista = None

    def imprimir(self, espacio):
        print espacio, "    variable:"
        self.identificador.imprimir(espaciacion(espacio))
        if not(self.lista is None):
            self.lista.imprimir(espaciacion(espacio))

"""
Clase que nos define el arbol para la lista de comportamiento
"""


class Arbol_Lista_Comportamientos(Arbol_Instruccion):

    def __init__(self, comportamiento, lista):
        if (not(comportamiento is None) and not(lista is None)):
            self.comportamiento = comportamiento
            self.lista = lista
        elif ((lista is None) and not(comportamiento is None)):
            self.comportamiento = comportamiento
            self.lista = None
        else:
            self.comportamiento = None
            self.lista = None

    def imprimir(self, espacio):
        if (not(self.comportamiento is None) and not(self.lista is None)):
            self.comportamiento.imprimir(espaciacion(espacio))
            self.lista.imprimir(espaciacion(espacio))
        elif(self.lista is None):
            self.comportamiento.imprimir(espaciacion(espacio))

"""
Clase que nos define el arbol para un comportamiento
"""


class Arbol_Comportamiento(Arbol_Instruccion):

    def __init__(self, condicion, instruccion):
        self.arbol_instruccion = Arbol_Instruccion('on')
        self.condicion = condicion
        self.instruccion = instruccion

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        self.condicion.imprimir(espaciacion(espacio))
        self.instruccion.imprimir(espaciacion(espacio))


"""
Clase que nos define el arbol que nos verificara una Expresion o Condicion
"""


class Arbol_Expresion_Condicion(Arbol_Instruccion):

    def __init__(self, expresion_condicion):
        self.expresion_condicion = expresion_condicion

    def imprimir(self, espacio):
        self.expresion_condicion.imprimir(espaciacion(espacio))


"""
Clase que nos define el arbol para los modos de un robot
"""


class Arbol_Modo(Arbol_Instruccion):

    def __init__(self, estado):
        self.estado = Arbol_Instruccion(estado)

    def imprimir(self, espacio):
        self.estado.imprimir(espaciacion(espacio))

"""

"""


class Arbol_Estado_Movimiento(Arbol_Instruccion):

    def __init__(self, estado):
        self.estado = Arbol_Instruccion(estado)

    def imprimir(self, espacio):
        self.estado.imprimir(espaciacion(espacio))


"""
Clase que nos define el arbol para la lista de instrucciones
"""


class Arbol_Lista_Instrucciones(Arbol_Instruccion):

    def __init__(self, instruccion, lista_instrucciones):
        self.instruccion = instruccion

        if not(lista_instrucciones is None):
            self.lista_instrucciones = lista_instrucciones

    def imprimir(self, espacio):
        self.instruccion.imprimir(espaciacion(espacio))

        if not(self.lista_instrucciones is None):
            self.lista_instrucciones.imprimir(espaciacion(espacio))

"""
Clases que nos definen los arboles para las instrucciones
"""

"""
arbol para las instruccion Store
"""


class Arbol_Store(Arbol_Instruccion):

    def __init__(self, expresion):
        self.arbol_instruccion = Arbol_Instruccion('store')
        self.expresion = expresion

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        self.expresion.imprimir(espaciacion(espacio))


"""
arbol para las instruccion Collect
"""


class Arbol_Collect(Arbol_Instruccion):

    def __init__(self, identificador):
        self.arbol_instruccion = Arbol_Instruccion('collect')
        if (not(identificador is None)):
            self.identificador = identificador
        else:
            self.identificador = None

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        if(not(self.identificador) is None):
            self.identificador.imprimir(espaciacion(espacio))


"""
arbol para las instruccion drop
"""


class Arbol_Drop(Arbol_Instruccion):

    def __init__(self, expresion):
        self.arbol_instruccion = Arbol_Instruccion('drop')
        self.expresion = expresion

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        self.expresion.imprimir(espaciacion(espacio))

"""
arbol para las instruccion de movimiento
"""


class Arbol_Movimiento(Arbol_Instruccion):

    def __init__(self, direccion, expresion):
        self.direccion = direccion
        if(not(expresion is None)):
            self.expresion = expresion
        else:
            self.expresion = None

    def imprimir(self, espacio):
        self.direccion.imprimir(espaciacion(espacio))
        if(not(self.expresion) is None):
            self.expresion.imprimir(espaciacion(espacio))


"""
Clases que nos definen los arboles para la entrada y salida de los robots
"""

"""
arbol para las instruccion read
"""


class Arbol_Read(Arbol_Instruccion):

    def __init__(self, identificador):
        self.arbol_instruccion = Arbol_Instruccion('read')
        if (not(identificador is None)):
            self.identificador = identificador
        else:
            self.identificador = None

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        if(not(self.identificador) is None):
            self.identificador.imprimir(espaciacion(espacio))


"""
arbol para las instruccion Send
"""


class Arbol_Send(Arbol_Instruccion):

    def __init__(self):
        self.arbol_instruccion = Arbol_Instruccion('send')

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))


"""
arbol para las instruccion recieve
"""


class Arbol_Recieve(Arbol_Instruccion):

    def __init__(self, identificador):
        self.arbol_instruccion = Arbol_Instruccion('recieve')
        if (not(identificador is None)):
            self.identificador = identificador
        else:
            self.identificador = None

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        if(not(self.identificador) is None):
            self.identificador.imprimir(espaciacion(espacio))


class Arbol_Execute(Arbol_Instruccion):

    def __init__(self, instrucciones_controlador):
        self.arbol_instruccion = Arbol_Instruccion('execute')
        self.instrucciones_controlador = instrucciones_controlador

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        self.instrucciones_controlador.imprimir(espaciacion(espacio))


class Arbol_Lista_Instrucciones_Controlador(Arbol_Instruccion):

    def __init__(self, controlador, lista_instrucciones):
        self.controlador = controlador
        if (not(lista_instrucciones is None)):
            self.lista_instrucciones = lista_instrucciones
        else:
            self.lista_instrucciones = None

    def imprimir(self, espacio):
        self.controlador.imprimir(espaciacion(espacio))
        if(not(self.lista_instrucciones) is None):
            self.lista_instrucciones.imprimir(espaciacion(espacio))


class Arbol_Activate(Arbol_Instruccion):

    def __init__(self, lista):
        self.arbol_instruccion = Arbol_Instruccion('activate')
        self.lista = lista

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        self.lista.imprimir(espaciacion(espacio))


class Arbol_Advance(Arbol_Instruccion):

    def __init__(self, lista):
        self.arbol_instruccion = Arbol_Instruccion('advance')
        self.lista = lista

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        self.lista.imprimir(espaciacion(espacio))


class Arbol_Deactivate(Arbol_Instruccion):

    def __init__(self, lista):
        self.arbol_instruccion = Arbol_Instruccion('deactivate')
        self.lista = lista

    def imprimir(self, espacio):
        self.arbol_instruccion.imprimir(espaciacion(espacio))
        self.lista.imprimir(espaciacion(espacio))


class Arbol_If_Else(Arbol_Instruccion):

    def __init__(self, condicion, instruccion1, instruccion2):
        self.arbol_instruccion1 = Arbol_Instruccion('if')
        self.condicion = condicion
        self.instruccion1 = instruccion1

        if (not(instruccion2 is None)):
            self.arbol_instruccion2 = Arbol_Instruccion('else')
            self.instruccion2 = instruccion2
        else:
            self.arbol_instruccion2 = None
            self.instruccion2 = None

    def imprimir(self, espacio):
        print espacio, "Condicional If-Else"
        print espacio, "guardia:"
        self.condicion.imprimir(espaciacion(espacio))
        print espacio, "exito:"
        for i in self.instruccion1:
            i.imprimir(espaciacion(espacio))
        print espacio, "fracaso:"
        for i in self.instruccion2:
            i.imprimir(espaciacion(espacio))


class Arbol_While(Arbol_Instruccion):

    def __init__(self, condicion, instruccion):
        self.arbol_instruccion = Arbol_Instruccion('while')
        self.condicion = condicion
        self.instruccion = instruccion

    def imprimir(self, espacio):
        print espacio, "While"
        print espacio, "    Condicion"
        self.condicion.imprimir(espaciacion(espacio))
        print espacio, "    Intruccion"
        for i in self.instruccion:
            i.imprimir(espaciacion(espacio))
