#!/usr/bin/env python   
# -*- encoding: utf-8 -*-

################################################################################
#                      ANÁLISIS LEXICOGRÁFICO - LENGUAJE BOT                   #
#                                                                              #
# Integrantes:                                                                 #
# 09-11133 Jorge Luis León                                                     #
# 10-10122 Maria Esther Carrillo                                               #
#                                                                              #
# Descripción:                                                                 #
# Módulo que contiene la funciones necesarias para el análisis lexicográfico   #
# del lenguaje BOT.                                                            #
#                                                                              #
################################################################################ 

import sys
import ply.lex as lex


class LexBot(object):

    # Arreglo que tendra los errores encontrados por el analizador
    # lexicografico.
    listaDeErrores = []

    # Diccionario que contiene las palabras reservadas del lenguaje BOT
    reservadas = {
        'advance'       : 'TkAdvance',
        'activate'      : 'TkActivate',
        'activation'    : 'TkActivation',
        'as'            : 'TkAs',
        'bool'          : 'TkBool',
        'bot'           : 'TkBot',
        'char'          : 'TkChar',
        'collect'       : 'TkCollect',
        'create'        : 'TkCreate',
        'deactivate'    : 'TkDeactivate',
        'deactivation'  : 'TkDeactivation',
        'default'       : 'TkDefault',
        'down'          : 'TkDown',
        'drop'          : 'TkDrop',
        'else'          : 'TkElse',
        'end'           : 'TkEnd',
        'execute'       : 'TkExecute',
        'false'         : 'TkFalse',
        'if'            : 'TkIf',
        'int'           : 'TkInt',
        'left'          : 'TkLeft',
        'me'            : 'TkMe',
        'on'            : 'TkOn',
        'read'          : 'TkRead',
        'receive'       : 'TkReceive',
        'right'         : 'TkRight',
        'send'          : 'TkSend',
        'store'         : 'TkStore',
        'true'          : 'TkTrue',
        'up'            : 'TkUp',
        'while'         : 'TkWhile'
    }

    # Lista que contiene todos los tokens del lenguaje,
    # al final de la lista se agregara el diccionario con
    # las palabras reservadas para tener obtener todos
    # los tokens de BOT.
    tokens = [
        'TkComa',
        'TkConjuncion',
        'TkDistinto',
        'TkDisyuncion',
        'TkDiv',
        'TkDosPuntos',
        'TkIgual',
        'TkMayor',
        'TkMayorIgual',
        'TkMenor',
        'TkMenorIgual',
        'TkMod',
        'TkMul',
        'TkNegacion',
        'TkParAbre',
        'TkParCierra',
        'TkPunto',
        'TkResta',
        'TkSuma',
        'TkCaracter',
        'TkComment',
        'TkIdent',
        'TkNum',
    ] + list(reservadas.values())

    # Definiciones de las expresiones regulares para cada token.
    t_TkComa        = r'\,'
    t_TkConjuncion  = r'\\\/'
    t_TkDistinto    = r'\/\='
    t_TkDisyuncion  = r'\/\\'
    t_TkDiv         = r'\/'
    t_TkDosPuntos   = r'\:'
    t_TkIgual       = r'\='
    t_TkMayor       = r'\>'
    t_TkMayorIgual  = r'\<\='
    t_TkMenor       = r'\<'
    t_TkMenorIgual  = r'\=\>'
    t_TkMod         = r'\%'
    t_TkMul         = r'\*'
    t_TkNegacion    = r'\~'
    t_TkParAbre     = r'\('
    t_TkParCierra   = r'\)'
    t_TkPunto       = r'\.'
    t_TkResta       = r'\-'
    t_TkSuma        = r'\+'
    t_ignore        = ' \t'            # Ignora espacios y tabuladores

    #
    # Función para manejar los caracteres
    #
    def t_TkCaracter(self, t):
        r'\'.\''
        return t

    #
    # Función para manejar los comentarios
    #
    def t_TkComment(self, t):
        r'((\$\-([^\-]|(\-)+[^\$])*\-\$)|(\$\$.*))'
        t.type = self.reservadas.get(t.value, 't_TkComment')
        pass

    #
    # Función para manejar los identificadores
    #
    def t_TkIdent(self, t):
        r'[a-zA-Z][a-zA-Z_0-9]*'
        t.type = self.reservadas.get(t.value, 'TkIdent')
        return t

    #
    # Función para transformar los números encontrados a enteros
    #
    def t_TkNum(self, t):
        r'\d{1,10}'
        try:
            t.value = int(t.value)
            return t
        except ValueError:
            t.value = float(t.value)

    #
    # Función para manejar los caracteres inesperados en el código
    #
    def t_error(self, t):
        self.listaDeErrores.append(t)
        t.lexer.skip(1)

    #
    # Función para manejar los saltos de línea
    #
    def t_TkSaltoDeLinea(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    #
    # Función que se encarga de calcular y retornar la columna
    # donde se encuentra el token
    #
    def token_columna(self, input, token):
        final = input.rfind('\n', 0, token.lexpos)
        if (final < 0):
            final = 1
            columna = 1
        else:
            columna = (token.lexpos - final)
        return columna

    #
    # Función que se encarga de construir el lexer
    #
    def build(self, **kwargs):

        self.lexer = lex.lex(module=self, **kwargs)

    #
    # Función que simula el analizador lexicográfico del lenguaje BOT
    #
    def analizador_lexicografico(self, archivo_texto):
        # Lista donde se guardaran los tokens encontrados
        tokens = []

        try:

            # Abpertura del archivo de entrada
            archive = open(archivo_texto)
            data = archive.read()

            # Construcción del lexer
            self.lexer.input(data)

            # Se recorre el archivo para ir obteniendo
            # todos los tokens y guardarlos en token[]
            while True:
                tok = self.lexer.token()
                if not tok:
                    break
                tokens.append(tok)

            # Cierre del archivo de texto
            archive.close()

        except IOError:

            print 'ERROR: No Se pudo abrir el archivo \"%s\" ' % archivo_texto
            exit()

        # Se verifica si se encontró algún error en el archivo,
        # de ser así, se imprimirá por pantalla la fila y columna
        # del mismo. Si no hay errores, se imprimirán todos los tokens
        # encontrados

        if (len(self.listaDeErrores) == 0):

            for i in tokens:
                if (i.type == 'TkIdent') or (i.type == 'TkCaracter'):
                    x1 = "(Fila: %d, Columna: %d) %s: ('%s')"
                    print x1 % (i.lineno, self.token_columna(data, i), i.type, i.value)
                elif (i.type == 'TkNum'):
                    x2 = "(Fila: %d, Columna: %d) %s: ('%d')"
                    print x2 % (i.lineno, self.token_columna(data, i), i.type, i.value)
                else:
                    x3 = "(Fila: %d, Columna: %d) %s"
                    print x3 % (i.lineno, self.token_columna(data, i), i.type)

            return 0

        else:

            for error in self.listaDeErrores:
                x = "ERROR, se encontro un Caracter inesperado \"%s\" (Linea: %d, Columna: %d)"
                print x % (error.value[0],error.lineno, self.token_columna(data,error))
                
            return 1

