#!/usr/bin/env python 
# -*- encoding: utf-8 -*-

################################################################################
#                      ANÁLISIS SINTÁCTICO - LENGUAJE BOT                      #
#                                                                              #
# Integrantes:                                                                 #
# 09-11133 Jorge Luis León                                                     #
# 10-10122 Maria Esther Carrillo                                               #
#                                                                              #
# Descripción:                                                                 #
# Módulo que contiene las clases con los procedimientos necesarios a ser       #
# utilizados por el Analizador Sintáctico (Parser.py) para construir el árbol  #
# sintáctico abstracto.                                                        #
#                                                                              #
#                                                                              #
################################################################################ 


import sys

# Diccionario para símbolos unarios
simbolos_unarios = {
    "~": "Negacion",
    "-": "Negativo",
}

# Diccionario para símbolos binarios
simbolos_binarios = {
    "+": "Suma",
    "-": "Resta",
    "*": "Multiplicacion",
    "/": "Division",
    "%": "Modulo",
    "/\\": "Conjuncion",
    "\/": "Disyuncion",
    "<": "Menor",
    "<=": "Menor o igual",
    ">": "Mayor",
    ">=": "Mayor o igual",
    "/=": "Distinto",
    "=": "Igual",
    }

#
# Funcion que se encarga de la espaciación adecuada en
# la impresión del árbol
#
def espaciacion(espacio):
    i = 0
    while (i < 4):
        espacio += " "
        i += 1
    return espacio


########### ÁRBOLES DE EXPRESIONES ###########

#
# Clase que maneja los árboles de expresiones de tipo
# booleano, caracter, entero, etc.
#
class Arbol_Expresion(object):

    def __init__(self, expresion):
        self.expresion = expresion

    def imprimir(self, espacio):
        print espacio, "Expresion"
        print espacio, "    Valor=", self.expresion
        
#
# Clase que maneja los árboles de expresiones unarias.
#
class Arbol_Unario(Arbol_Expresion):

    def __init__(self, operador, expresion):
        self.operador = operador
        self.expresion = expresion

    def imprimir(self, espacio):
        operador_unario = simbolos_unarios[self.operador]
        if (self.expresion != None):
            print espacio, "Operador:", "'"+str(self.operador)+"'", operador_unario
            print espacio, "   Expresion"
            self.expresion.imprimir(espaciacion(espacio))

#
# Clase que maneja los árboles de expresiones binarias.
#            
class Arbol_Binario(Arbol_Expresion):

    def __init__(self, expresion1, operador, expresion2):
        self.expresion1 = expresion1
        self.expresion2 = expresion2
        self.operador = operador

    def imprimir(self, espacio):
        operador_binario = simbolos_binarios[self.operador]
        print espacio, "Operador:", "'"+str(self.operador)+"'", operador_binario
        if (self.expresion1 != None):
            print espacio, "  Operando izquierdo:"
            self.expresion1.imprimir(espaciacion(espacio))
            print espacio, "  Operando derecho:"
        if (self.expresion2 != None):
            self.expresion2.imprimir(espaciacion(espacio))

########### ÁRBOLES DE LA ESTRUCTURA PRINCIPAL DEL PROGRAMA ###########            

#
# Clase encargada de imprimir las hojas de los árboles y las
# instrucciones de las palabras reservadas. 
#
class Instruccion_BOT(object):

    def __init__(self, instruccion):
        self.instruccion = instruccion

    def imprimir(self, espacio):
        print self.instruccion

#
# Clase que maneja el árbol del programa principal y programas
# anidados en el mismo
#  
class Arbol_Programa(Instruccion_BOT):

    def __init__(self, lista_declaraciones, execute):
        self.execute = execute
        if (lista_declaraciones != None):
            self.instruccion_BOT = Instruccion_BOT('create')
            self.lista_declaraciones = lista_declaraciones
        else:
            self.lista_declaraciones = None

    def imprimir(self, espacio):
        if (self.lista_declaraciones != None):
            self.instruccion_BOT.imprimir(espaciacion(espacio))
            self.lista_declaraciones.imprimir(espaciacion(espacio))
            self.execute.imprimir(espaciacion(espacio))
        else:
            self.execute.imprimir(espaciacion(espacio))


#
# Clase que maneja el árbol la instrucción execute, que 
# indica el principio de las instrucciones del controlador 
# 
class Arbol_Execute(Instruccion_BOT):

    def __init__(self, instrucciones_controlador):
        self.instruccion_BOT = Instruccion_BOT('execute')
        self.instrucciones_controlador = instrucciones_controlador

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.instrucciones_controlador.imprimir(espaciacion(espacio))


########### ÁRBOLES DE LISTAS Y CLASES ASOCIADAS ###########

#
# Clase que maneja el árbol de las listas de declaraciones
# de los robots
#  
class Arbol_Lista_Declaraciones(Instruccion_BOT):

    def __init__(self, declaracion, lista_declaraciones):
        self.declaracion = declaracion
        if (not(lista_declaraciones == None)):
            self.lista_declaraciones = lista_declaraciones
        else:
            self.lista_declaraciones = None

    def imprimir(self, espacio):
        self.declaracion.imprimir(espaciacion(espacio))
        if (not(self.lista_declaraciones == None)):
            self.lista_declaraciones.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol de declaraciones de los robots
#  
class Arbol_Bot(Instruccion_BOT):

    def __init__(self, tipo, lista_identificadores, lista_comportamientos):
        self.tipo = tipo
        self.instruccion_BOT = Instruccion_BOT('bot')
        self.lista_identificadores = lista_identificadores
        if (not(lista_comportamientos == None)):
            self.lista_comportamientos = lista_comportamientos
        else:
            self.lista_comportamientos = None

    def imprimir(self, espacio):
        self.tipo.imprimir(espaciacion(espacio))
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.lista_identificadores.imprimir(espaciacion(espacio))
        if (not(self.lista_comportamientos == None)):
            self.lista_comportamientos.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol de la lista de identificadores
# de los robots
#  
class Arbol_Lista_Identificadores(Instruccion_BOT):

    def __init__(self, identificador, lista):
        self.identificador = identificador

        if not(lista == None):
            self.lista = lista
        else:
            self.lista = None

    def imprimir(self, espacio):
        print espacio, "    variable:"
        self.identificador.imprimir(espaciacion(espacio))
        if not(self.lista == None):
            self.lista.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la lista de comportamientos de los
# robots
#  
class Arbol_Lista_Comportamientos(Instruccion_BOT):

    def __init__(self, comportamiento, lista):
        self.comportamiento = comportamiento
        if (not(lista == None)):
            self.lista = lista
        else:
            self.lista = None

    def imprimir(self, espacio):
        self.comportamiento.imprimir(espaciacion(espacio))

        if (not(self.lista == None)):
            self.lista.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol los comportamientos de los robots
# 
class Arbol_Comportamiento(Instruccion_BOT):

    def __init__(self, condicion, instruccion):
        self.instruccion_BOT = Instruccion_BOT('on')
        self.condicion = condicion
        self.instruccion = instruccion

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.condicion.imprimir(espaciacion(espacio))
        self.instruccion.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol de los tipos asociados a los robots
#  
class Arbol_Tipo(Instruccion_BOT):

    def __init__(self, tipo):
        self.tipo = Instruccion_BOT(tipo)

    def imprimir(self, espacio):
        self.tipo.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol de los modos de comportamiento
# de los robots
# 
class Arbol_Modo(Instruccion_BOT):

    def __init__(self, estado):
        self.estado = Instruccion_BOT(estado)

    def imprimir(self, espacio):
        self.estado.imprimir(espaciacion(espacio))

#
# Clase que maneja el arbol de las listas de intrucciones
# tanto en el controlador como en los robots
# 
class Arbol_Lista_Instrucciones(Instruccion_BOT):

    def __init__(self, instruccion, lista_instrucciones):
        self.instruccion = instruccion

        if (not(lista_instrucciones == None)):
            self.lista_instrucciones = lista_instrucciones
        else:
            self.lista_instrucciones = None

    def imprimir(self, espacio):
        self.instruccion.imprimir(espaciacion(espacio))

        if (not(self.lista_instrucciones == None)):
            self.lista_instrucciones.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol de la lista de instrucciones del
# controlador
# 
class Arbol_Lista_Instrucciones_Controlador(Instruccion_BOT):

    def __init__(self, controlador, lista_instrucciones):
        self.controlador = controlador
        if (not(lista_instrucciones == None)):
            self.lista_instrucciones = lista_instrucciones
        else:
            self.lista_instrucciones = None

    def imprimir(self, espacio):
        self.controlador.imprimir(espaciacion(espacio))
        if(not(self.lista_instrucciones) == None):
            self.lista_instrucciones.imprimir(espaciacion(espacio))


########### ÁRBOLES DE INSTRUCCIONES DEL CONTROLADOR ###########

#
# Clase que maneja el árbol la instrucción activate 
# 
class Arbol_Activate(Instruccion_BOT):

    def __init__(self, lista):
        self.instruccion_BOT = Instruccion_BOT('activate')
        self.lista = lista

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.lista.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción advance 
# 
class Arbol_Advance(Instruccion_BOT):

    def __init__(self, lista):
        self.instruccion_BOT = Instruccion_BOT('advance')
        self.lista = lista

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.lista.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción deactivate 
# 
class Arbol_Deactivate(Instruccion_BOT):

    def __init__(self, lista):
        self.instruccion_BOT = Instruccion_BOT('deactivate')
        self.lista = lista

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.lista.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción condicional  
# 
class Arbol_If_Else(Instruccion_BOT):

    def __init__(self, condicion, instruccion1, instruccion2):
        self.intruccion_bot1 = Instruccion_BOT('if')
        self.condicion = condicion
        self.instruccion1 = instruccion1

        if (not(instruccion2 == None)):
            self.intruccion_bot2 = Instruccion_BOT('else')
            self.instruccion2 = instruccion2
        else:
            self.intruccion_bot2 = None
            self.instruccion2 = None

    def imprimir(self, espacio):
        print espacio, "Condicional If-Else"
        print espacio, "guardia:"
        self.condicion.imprimir(espaciacion(espacio))
        print espacio, "exito:"
        self.instruccion1.imprimir(espaciacion(espacio))
        if (not(self.instruccion2 == None)):
            print espacio, "fracaso:"            
            self.instruccion2.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción iteración
# indeterminada 
# 
class Arbol_While(Instruccion_BOT):

    def __init__(self, condicion, instruccion):
        self.instruccion_BOT = Instruccion_BOT('while')
        self.condicion = condicion
        self.instruccion = instruccion

    def imprimir(self, espacio):
        print espacio, "While"
        print espacio, "    Condicion"
        self.condicion.imprimir(espaciacion(espacio))
        print espacio, "    Intruccion"
        self.instruccion.imprimir(espaciacion(espacio))


########### ÁRBOLES DE INSTRUCCIONES DE ROBOTS ###########

#
# Clase que maneja el árbol la instrucción store 
# 
class Arbol_Store(Instruccion_BOT):

    def __init__(self, expresion):
        self.instruccion_BOT = Instruccion_BOT('store')
        self.expresion = expresion

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.expresion.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción collect 
# 
class Arbol_Collect(Instruccion_BOT):

    def __init__(self, identificador):
        self.instruccion_BOT = Instruccion_BOT('collect')
        if (not(identificador == None)):
            self.identificador = identificador
        else:
            self.identificador = None

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        if(not(self.identificador) == None):
            self.identificador.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción drop 
# 
class Arbol_Drop(Instruccion_BOT):

    def __init__(self, expresion):
        self.instruccion_BOT = Instruccion_BOT('drop')
        self.expresion = expresion

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        self.expresion.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol las posibles instrucciones
# de movimiento de un robot
# 
class Arbol_Movimiento(Instruccion_BOT):

    def __init__(self, direccion, expresion):
        self.direccion = direccion
        if(not(expresion == None)):
            self.expresion = expresion
        else:
            self.expresion = None

    def imprimir(self, espacio):
        self.direccion.imprimir(espaciacion(espacio))
        if(not(self.expresion) == None):
            self.expresion.imprimir(espaciacion(espacio))

    #
# Clase que maneja el árbol de las instrucciones de 
# movimiento de los robots
# 
class Arbol_Direccion(Instruccion_BOT):

    def __init__(self, estado):
        self.estado = Instruccion_BOT(estado)

    def imprimir(self, espacio):
        self.estado.imprimir(espaciacion(espacio))


#
# Clase que maneja el árbol la instrucción read 
# 
class Arbol_Read(Instruccion_BOT):

    def __init__(self, identificador):
        self.instruccion_BOT = Instruccion_BOT('read')
        if (not(identificador == None)):
            self.identificador = identificador
        else:
            self.identificador = None

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        if(not(self.identificador) == None):
            self.identificador.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción send 
# 
class Arbol_Send(Instruccion_BOT):

    def __init__(self, identificador):
        self.instruccion_BOT = Instruccion_BOT('send')
        if (not(identificador == None)):
            self.identificador = identificador
        else:
            self.identificador = None

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        if(not(self.identificador) == None):
            self.identificador.imprimir(espaciacion(espacio))

#
# Clase que maneja el árbol la instrucción receive 
# 
class Arbol_Receive(Instruccion_BOT):

    def __init__(self, identificador):
        self.instruccion_BOT = Instruccion_BOT('receive')
        if (not(identificador == None)):
            self.identificador = identificador
        else:
            self.identificador = None

    def imprimir(self, espacio):
        self.instruccion_BOT.imprimir(espaciacion(espacio))
        if(not(self.identificador) == None):
            self.identificador.imprimir(espaciacion(espacio))



