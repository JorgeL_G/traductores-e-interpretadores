#!/usr/bin/env python	
# -*- encoding: utf-8 -*-

################################################################################
#                      ANÁLISIS SINTÁCTICO - LENGUAJE BOT                      #
#                                                                              #
# Integrantes:                                                                 #
# 09-11133 Jorge Luis León                                                     #
# 10-10122 Maria Esther Carrillo                                               #
#                                                                              #
# Descripción:                                                                 #
# Módulo que contiene la definición de las gramáticas para la construcción del #                     
# árbol sintáctico abstracto.                                                  #
#                                                                              #
################################################################################ 


import sys

import ply.yacc as yacc


from LexBot import LexBot
from Clases import *

cont = 0
archivo = ""


########### ESTRUCUTRA PRINCIPAL DEL PROGRAMA ###########

#
# Función para la estructura principal del programa
#
def p_inicio_programa(p):
  	'''PROGRAMA : TkCreate LISTA_DECLARACIONES EXECUTE 
              	| EXECUTE'''
	global cont
	cont = cont + 1

  	if (p[1] == 'create'):
  		# Se agregó este condicional para poder imprimir el create
  		# de la primera lista de declaraciones
  		if (cont == 1):
  			print "create"
  		p[0] = Arbol_Programa(p[2],p[3])
 	else: 
  		p[0] = Arbol_Programa(None,p[1])

#
# Función para la instrucción execute que indica el
# inicio de las instrucciones del controlador
#
def p_execute(p):
   	'''EXECUTE : TkExecute LISTA_INSTRUCCIONES_CONTROLADOR TkEnd '''

	p[0] = Arbol_Execute(p[2])


########### LISTAS DE DECLARACIONES DE ROBOTS ###########

#
# Función para las listas de declaraciones de diferentes robots
#
def p_lista_declaraciones(p):
  	'''LISTA_DECLARACIONES : DECLARACION LISTA_DECLARACIONES
               			   | DECLARACION'''
          				
  	if len(p) == 3:
  		p[0] = Arbol_Lista_Declaraciones(p[1],p[2])   
  	else:
  		p[0] = Arbol_Lista_Declaraciones(p[1],None)

#
# Función para las declaraciones de robots
#
def p_declaracion(p):
  	'''DECLARACION   : TIPO TkBot LISTA_IDENTIFICADORES LISTA_COMPORTAMIENTOS TkEnd
  					 | TIPO TkBot LISTA_IDENTIFICADORES TkEnd'''

  	if len(p) == 6:
  		p[0] = Arbol_Bot(p[1],p[3],p[4])   
  	else:
  		p[0] = Arbol_Bot(p[1],p[3],None)

#
# Función para los tipos asociados a los robots
#
def p_tipo(p):
  	'''TIPO : TkInt
  		   	| TkBool
           	| TkChar'''

  	p[0] = Arbol_Tipo(p[1])

#
# Función para identificadores de robots
#
def p_id_list(p):
  	'''LISTA_IDENTIFICADORES : IDENTIFICADOR TkComa LISTA_IDENTIFICADORES
              				 | IDENTIFICADOR'''

	if len(p) == 4:
	    p[0] = Arbol_Lista_Identificadores(p[1],p[3])
	else : 
	    p[0] = Arbol_Lista_Identificadores(p[1],None)

#
# Función para el identificador de un robot
#
def p_identificador(p):
  	'''IDENTIFICADOR : TkIdent'''

  	p[0] = Arbol_Expresion(p[1])

#
# Función para la listas de comportamientos de robots
#
def p_lista_comportamientos(p):
  	'''LISTA_COMPORTAMIENTOS : COMPORTAMIENTO LISTA_COMPORTAMIENTOS
  							 | COMPORTAMIENTO'''

	if len(p) == 3:
		p[0] = Arbol_Lista_Comportamientos(p[1],p[2])
	else:
		p[0] = Arbol_Lista_Comportamientos(p[1],None)

#
# Función para comportamientos de un robot
#
def p_comportamiento(p):
  	'''COMPORTAMIENTO : TkOn MODO TkDosPuntos LISTA_INSTRUCCIONES TkEnd 
           			  | TkOn EXPRESION TkDosPuntos LISTA_INSTRUCCIONES TkEnd'''

	p[0] = Arbol_Comportamiento(p[2],p[4])


#
# Función para parte de las condiciones de un robot
#
def p_modo(p):
  	''' MODO : TkActivation
             | TkDeactivation
             | TkDefault'''

	p[0] = Arbol_Modo(p[1])


########### LISTAS DE INSTRUCCIONES DE ROBOTS ###########

#
# Función para las listas de instrucciones asociadas a un comportamiento
#
def p_lista_instrucciones(p):
  	'''LISTA_INSTRUCCIONES : INSTRUCCIONES LISTA_INSTRUCCIONES
                    	   | INSTRUCCIONES'''

  	if len(p) == 3:
  		p[0] = Arbol_Lista_Instrucciones(p[1],p[2])
  	else:
  		p[0] = Arbol_Lista_Instrucciones(p[1],None)

#
# Función para las instrucciones posibles de un robot
#
def p_instrucciones(p):
  	'''INSTRUCCIONES : RECEIVE
  					 | STORE
  					 | COLLECT 
  					 | DROP
  					 | MOVIMIENTO
  					 | E_S'''

  	p[0] = p[1]

#
# Función para la instrucción 'receive'
#
def p_receive(p):
	'''RECEIVE : TkReceive TkPunto'''

	p[0] = Arbol_Receive(None)

#
# Función para la instrucción 'store'
#
def p_store(p):
	'''STORE : TkStore TkMe EXPRESION TkPunto
			 | TkStore EXPRESION TkPunto'''
	
	if (len(p) == 5):
		p[0] = Arbol_Store(p[3])
	else:
		p[0] = Arbol_Store(p[2])

#
# Función para la instrucción 'drop'
#
def p_drop(p):
	'''DROP : TkDrop TkMe EXPRESION TkPunto
			| TkDrop EXPRESION TkPunto'''
	
	if (len(p) == 5):
		p[0] = Arbol_Drop(p[3])
	else:
		p[0] = Arbol_Drop(p[2])

#
# Función para la instrucción 'collect'
#
def p_collect(p):
	'''COLLECT : TkCollect TkAs IDENTIFICADOR TkPunto
			   | TkCollect TkPunto'''
	
	if (len(p) == 5): 
		p[0] = Arbol_Collect(p[3])
	else:
		p[0] = Arbol_Collect(None)

#
# Función para las posibles instrucciones de movimiento
#
def p_movimiento(p):
	'''MOVIMIENTO : DIRECCION EXPRESION TkPunto
				  | DIRECCION TkPunto'''
	
	if len(p) == 4: 
		p[0] = Arbol_Movimiento(p[1],p[2])
	else:
		p[0] = Arbol_Movimiento(p[1],None)
#
# Función para las direcciones de movimiento
#
def p_direccion(p):
  	'''DIRECCION : TkUp
          		 | TkDown
  				 | TkLeft 
          		 | TkRight'''

	p[0] = Arbol_Direccion(p[1])

#
# Función para las instrucciones de entrada y salida
#
def p_entrada_salida(p):
	'''E_S : TkRead TkAs IDENTIFICADOR TkPunto
		   | TkRead TkPunto
		   | TkSend TkPunto'''
	
	if (p[1] == 'read'):
		if len(p) == 5:
			p[0] = Arbol_Read(p[3])
		else:
			p[0] = Arbol_Read(None)
	else:
		p[0] = Arbol_Send(None)	


########### LISTAS DE INSTRUCCIONES DEL CONTROLADOR ###########
		    
#
# Función para la lista de instrucciones del controlador
#
def p_lista_instrucciones_controlador(p):
  	'''LISTA_INSTRUCCIONES_CONTROLADOR : INSTRUCCIONES_CONTROLADOR LISTA_INSTRUCCIONES_CONTROLADOR
                     				   | INSTRUCCIONES_CONTROLADOR'''
	
	if len(p) == 2 :
		p[0] = Arbol_Lista_Instrucciones_Controlador(p[1],None)
	else :
		p[0] = Arbol_Lista_Instrucciones_Controlador(p[1],p[2])

#
# Función para las posibles instrucciones del controlador
#
def p_instrucciones_controlador(p):
	'''INSTRUCCIONES_CONTROLADOR : ACTIVATE
								 | ADVANCE
								 | DEACTIVATE
								 | IF
								 | WHILE
								 | PROGRAMA'''
	
	p[0] = p[1]

#
# Función para la instrucción 'activate'
#
def p_activate (p):
	'''ACTIVATE : TkActivate LISTA_IDENTIFICADORES TkPunto'''

	p[0] = Arbol_Activate(p[2])

#
# Función para la instrucción 'deactivate'
#
def p_deactivate (p):
	'''DEACTIVATE : TkDeactivate LISTA_IDENTIFICADORES TkPunto'''

	p[0] = Arbol_Deactivate(p[2])

#
# Función para la instrucción 'advance'
#
def p_advance (p):
	'''ADVANCE : TkAdvance LISTA_IDENTIFICADORES TkPunto'''

	p[0] = Arbol_Advance(p[2])

#
# Función para la instrucción condicional
#
def p_if_else (p):
	'''IF : TkIf EXPRESION TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkElse TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkEnd
		  | TkIf EXPRESION TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkEnd'''
	
	if p[5] == 'else' :
	  	p[0] = Arbol_If_Else(p[2],p[4],p[7])
	else : 
  		p[0] = Arbol_If_Else(p[2],p[4],None)

#
# Función para la instrucción iteración indeterminada
#
def p_while (p):
	'''WHILE : TkWhile EXPRESION TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkEnd'''

	p[0] = Arbol_While(p[2],p[4])


########### EXPRESIONES ###########

# Lista de precedencia de los operandos
precedencia = (
  ('left','TkMayor','TkMayorIgual','TkMenor','TkMenorIgual','TkIgual','TkDistinto'),
  ('left','TkSuma','TkResta'),
  ('left','TkDiv','TkMult','TkMod'),
  ('left','TkDisyuncion','TkConjuncion'),
  ('left','TkNegacion','TkMenos'), 
)

#
# Función para las mínimas expresiones
#
def p_expresion(p):
    '''EXPRESION : TkTrue
                 | TkFalse
                 | TkNum
                 | TkIdent
                 | TkCaracter
                 | TkMe'''

    if len(p) == 2:
        p[0] = Arbol_Expresion(p[1])

#
# Función para las expresiones unarias
#
def p_expresion_unaria(p):
    '''EXPRESION  : TkNegacion EXPRESION
                  | TkResta    EXPRESION'''

    p[0] = Arbol_Unario(p[1], p[2])

#
# Función para la instrucción 'collect'
#
def p_expresion_binaria(p):
    '''EXPRESION :   EXPRESION TkSuma         EXPRESION
                   | EXPRESION TkResta        EXPRESION
                   | EXPRESION TkMul          EXPRESION
                   | EXPRESION TkDiv          EXPRESION
                   | EXPRESION TkMod          EXPRESION
                   | EXPRESION TkConjuncion   EXPRESION 
                   | EXPRESION TkDisyuncion   EXPRESION
                   | EXPRESION TkMayor        EXPRESION
                   | EXPRESION TkMayorIgual   EXPRESION
                   | EXPRESION TkMenor        EXPRESION
                   | EXPRESION TkMenorIgual   EXPRESION
                   | EXPRESION TkIgual        EXPRESION           
                   | EXPRESION TkDistinto     EXPRESION'''

    p[0] = Arbol_Binario(p[1], p[2], p[3])

#
# Función para las expresiones parentizadas
#
def p_expresion_parentesis(p):
    ''' EXPRESION : TkParAbre EXPRESION TkParCierra'''

    p[0] = p[2]


########### MANEJO DE ERRORES SINTÁCTICOS ###########

#
# Función para el manejo de errores de sintaxis
#
def p_error(p):
	if p is not None:
		# A pesar de encontrar el error, no imprime correctamente la línea del error sintáctico	
		print "ERROR DE SINTAXIS:"+  " Token Inesperado '" + str(p.value) + "' (Linea: " + str(p.lineno) + ")"  
		exit()		
	else:
	    print "Error de sintaxis: falta de token lo que impide cumplimiento de gramatica"
	    exit()

########### ANÁLISIS SINTÁCTICO ###########

#
# Función que simula el analizador sintáctico del lenguaje BOT
#
def analizador_sintactico(archivo_texto):

	try:
		tokens = LexBot.tokens
		espacio = " "

		lexer_bot = LexBot()
		lexer_bot.build()
		lexer_bot.analizador_lexicografico(archivo_texto)

		# Apertura del archivo de entrada
		f = open(archivo_texto)

		# Lectura del archivo de entrada
		data = f.read()

		# Construcción del parser
		parser = yacc.yacc()
		result = parser.parse(data)

		# Impresión de los árboles de instrucciones
		if (result != None):
			if (result.lista_declaraciones != None):
				result.lista_declaraciones.imprimir(espaciacion(espacio))
				result.execute.imprimir(espaciacion(espacio))
			else:
				result.execute.imprimir(espaciacion(espacio))


		# Cierre del archivo de texto
		f.close()

	except IOError:

		print 'ERROR: No se pudo abrir el archivo de texto \"%s\"' % archivo_texto
		exit()


