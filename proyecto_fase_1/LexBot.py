#!/usr/bin/python
# -*- encoding: utf-8 -*-

"""
TRADUCTORES E INTERPRETADORES (CI-3725)
Etapa I
Analisis Lexicografico

Profesor Ricardo Monascal

Autores:
Jorge Luis Leon       09-11133
Maria Esther Carrillo 10-10122
"""

import sys
import ply.lex as lex


class LexBot(object):

    # Arreglo que tendra los errores encontrados por el analisis
    # lexicografico.
    listaDeErrores = []

    # Diccionario que contiene las palabras reservadas del lenguaje BOT
    reservadas = {
        'advance': 'TkAvance',
        'activate': 'TkActivate',
        'activation': 'TkActivation',
        'as': 'TkAs',
        'bool': 'TkBool',
        'bot': 'TkBot',
        'char': 'TkChar',
        'collect': 'TkCollect',
        'create': 'TkCreate',
        'deactivate': 'TkDeactivate',
        'deactivation': 'TkDeactivation',
        'default': 'TkDefault',
        'down': 'TkDown',
        'drop': 'TkDrop',
        'else': 'TkElse',
        'end': 'TkEnd',
        'execute': 'TkExecute',
        'false': 'TkFalse',
        'if': 'TkIf',
        'int': 'TkInt',
        'left': 'TkLeft',
        'me': 'TkMe',
        'on': 'TkOn',
        'receive': 'TkReceive',
        'right': 'TkRight',
        'send': 'TkSend',
        'store': 'TkStore',
        'true': 'TkTrue',
        'up': 'TkUp'
    }

    # Lista que contiene todos los tokens del lenguaje,
    # al final de la lista se agregara el diccionario con
    # las palabras reservadas para tener obtener todos
    # los tokens de BOT.

    tokens = [
        'TkComa',
        'TkConjuncion',
        'TkDisyuncion',
        'TkDiv',
        'TkDosPuntos',
        'TkIgual',
        'TkMayor',
        'TkMayotIgual',
        'TkMenor',
        'TkMenorIgual',
        'TkMod',
        'TkMul',
        'TkNegacion',
        'TkParAbre',
        'TkParCierra',
        'TkPunto',
        'TkResta',
        'TkSuma',
        'TkCaracter',
        'TkComment',
        'TkIdent',
        'TkNum',
    ] + list(reservadas.values())

    # Definiciones de las expresiones regulares para cada token.

    t_TkComa = r'\,'
    t_TkConjuncion = r'\\\/'
    t_TkDisyuncion = r'\/\\'
    t_TkDiv = r'\/'
    t_TkDosPuntos = r'\:'
    t_TkIgual = r'\='
    t_TkMayor = r'\>'
    t_TkMayotIgual = r'\<\='
    t_TkMenor = r'\<'
    t_TkMenorIgual = r'\=\>'
    t_TkMod = r'\%'
    t_TkMul = r'\*'
    t_TkNegacion = r'\~'
    t_TkParAbre = r'\('
    t_TkParCierra = r'\)'
    t_TkPunto = r'\.'
    t_TkResta = r'\-'
    t_TkSuma = r'\+'
    t_ignore = ' \t'            # Ignora espacios y tabuladores

    """
    FUNCION TKCARACTER
    Se encarga de manejar los caracteres
    """

    def t_TkCaracter(self, t):
        r'\'.\''
        return t

    """
    FUNCION COMMENT
    Se encarga de manejar caracteres los comentarios, estos no
    se agregaran a la lista de tokens
    """

    def t_TkComment(self, t):
        r'((\$\-([^\-]|(\-)+[^\$])*\-\$)|(\$\$.*))'
        t.type = self.reservadas.get(t.value, 't_TkComment')
        pass

    """
    FUNCION TKIDENT
    Se encarga de manejar los identificadores
    """

    def t_TkIdent(self, t):
        r'[a-zA-Z][a-zA-Z_0-9]*'
        t.type = self.reservadas.get(t.value, 'TkIdent')
        return t

    """
    FUNCION TKNUM
    Se encarga de tranformar los numeros que se encuentren a enteros
    """

    def t_TkNum(self, t):
        r'[-+]?[0-9]*\.?[0-9]+'
        try:
            t.value = int(t.value)
            return t
        except ValueError:
            t.value = float(t.value)

    """
    FUNCION ERROR
    Se encarga de manejar caracteres inesperados en el codigo
    """

    def t_error(self, t):
        self.listaDeErrores.append(t)
        t.lexer.skip(1)

    """
    FUNCION NEWLINE
    Se encarga de manejar caracteres los saltos de linea
    """

    def t_TkNewLine(self, t):
        r'\n+'
        t.lexer.lineno += len(t.value)

    """
    FUNCION COLUMN_TOKEN
    Se encarga de calcular y retornar la columna de donde
    se encuentra el token
    """

    def Column_Token(self, input, token):
        endOfLine = input.rfind('\n', 0, token.lexpos)
        if (endOfLine < 0):
            endOfLine = 1
            column = 1
        else:
            column = (token.lexpos - endOfLine)
        return column

    """
    CONSTRUCTOR DEL LEXER
    """
    def build(self, **kwargs):

        self.lexer = lex.lex(module=self, **kwargs)

    """
    FUNCION LEXICOGRAPHICAL ANALYZER
    Se simulara el analizador lexicografico del lenguaje BOT
    """

    def lexicographical_analyzer(self, textFile):
        # Lista donde se guardaran los tokens encontrados
        tokens = []

        try:

            # Abriremos el archivo para despues leerlo
            archive = open(textFile)
            data = archive.read()

            # Construimos el lexer
            self.lexer.input(data)

            # Se recorrera el archivo para ir obteniendo
            # todos los tokens y guardarlos en token[]

            while True:
                tok = self.lexer.token()
                if not tok:
                    break
                tokens.append(tok)

            # Cerramos el archivo analizado por el lexer
            archive.close()

        except IOError:

            print 'ERROR: No Se pudo abrir el archivo \"%s\" ' % textFile
            exit()

        # Se verificara si se encontro algun error en el archivo,
        # de ser asi, se imprimira por pantalla la fila y columna
        # del mismo. Si esto no ocurre, se daran todos los tokens
        # encontrados.

        if (len(self.listaDeErrores) == 0):

            for i in tokens:
                if (i.type == 'TkIdent') or (i.type == 'TkCaracter'):
                    x1 = "(Fila: %d, Columna: %d) %s: ('%s')"
                    print x1 % (i.lineno, self.Column_Token(data, i), i.type, i.value)
                elif (i.type == 'TkNum'):
                    x2 = "(Fila: %d, Columna: %d) %s: ('%d')"
                    print x2 % (i.lineno, self.Column_Token(data, i), i.type, i.value)
                else:
                    x3 = "(Fila: %d, Columna: %d) %s"
                    print x3 % (i.lineno, self.Column_Token(data, i), i.type)

            # Como no se encontraron errores se terminara el archivo
            # retornando el valor 0
            return 0

        else:

            for error in self.listaDeErrores:
                x = "ERROR, se encontro un Caracter inesperado \"%s\" (Linea: %d, Columna: %d)"
                print x % (error.value[0],error.lineno, self.Column_Token(data,error))

            # Como si se encontraron errores se terminara el archivo
            # retornando el valor 1

            return 1


def main():
    # Construimos el Lexer
    m = LexBot()
    m.build()
    if (len(sys.argv) > 1):
        m.lexicographical_analyzer(sys.argv[1])
    else:
        print "Verifique que la sintaxis introducida es correcta.\n"
        print "./LexBot <nombre del archivo>\n"

if __name__ == "__main__":
    main()
