#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE BOOLEANO - LENGUAJE BOT                          #
#                                                                             #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de los tipos booleanos 	      #
###############################################################################


class Booleano:

    def __init__(self, valor):
        self.valor = valor
        self.type = "bool"
        
    def chequear(self, tablaSimb):
        return self.type

# END Booleano.py
