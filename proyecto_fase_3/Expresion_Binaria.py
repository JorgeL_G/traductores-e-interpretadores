#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                     CLASE EXPRESION_BINARIA - LENGUAJE BOT                  #       
#                       ⟨Expresion1⟩ operador ⟨Expresion2⟩                    #                     
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de las expresiones de tipo      #
# binaria.                                                                    #
###############################################################################

import Expresion_Negativo
import sys

class Expresion_Binaria:

    def __init__(self,expresion1,operador,expresion2):
        self.expresion1 = expresion1
        self.expresion2 = expresion2
        self.operador  = operador
        self.type = "exp_bin"

    def chequear_tipos(self,tipoExp1,tipoExp2):
        simbolos = ["+","-","*","/","%"]
        simbolos_rango = [">",">=","<","<="]
        simbolos_comparacion = ["==","/="]
        simbolos_boolos = ["/\\","\/"]


        e = "ERROR-EXP-BIN: Incompatibilidad de tipos para realizar la operacion \'%s\'."
        f = "ERROR-EXP-BIN: No se puede realizar la operacion \'%s\'."

        # Símbolos aritméticos
        if str(self.operador) in simbolos:

            if (tipoExp1 != tipoExp2):
                print e % self.operador
                sys.exit(1)

            # NUMBER
            if (tipoExp1 == "int"):
                return "int"

            else:
                print e % self.operador
                sys.exit(1)

        # Símbolos rango
        elif str(self.operador) in simbolos_rango:

            if (tipoExp1 != tipoExp2):
                print e % self.operador
                sys.exit(1)

            # NUMBER
            if (tipoExp1 == "int"):
                return "bool"

            else:
                print e % self.operador
                sys.exit(1) 

        # Símbolos comparación
        elif str(self.operador) in simbolos_comparacion:

            if (tipoExp1 != tipoExp2):
                print e % self.operador
                sys.exit(1)

            return "bool"        

        # Símbolos boolos
        elif str(self.operador) in simbolos_boolos:

            if (tipoExp1 != tipoExp2):
                print e % self.operador
                sys.exit(1)

            if (tipoExp1 == "bool"):
                return "bool"

            else:
                print e % self.operador
                sys.exit(1)


    def chequear(self,tablaSimb):
        nombreVariable1 = self.expresion1.chequear(tablaSimb)
        nombreVariable2 = self.expresion2.chequear(tablaSimb)

        # Verificamos que las dos expresiones son un literal numerico, si es asi, retornamos 'int'

        if (nombreVariable1 == 'int' and nombreVariable2 == 'int'):
            return 'int'

        # Verificamos que si la variable 1 o 2 es un entero, esta no este con operada con un booleano 
        if ((nombreVariable1 == 'int' and nombreVariable2 == 'bool') or (nombreVariable1 == 'bool' and nombreVariable2 == 'int')):
            msg = "ERROR-EXP-BIN: La expresion 1 es \'%s\' y la expresion 2 es \'%s\', no se pueden operar"
            e = msg % (nombreVariable1, nombreVariable2)
            print e
            sys.exit(1)

        # Verificamos que si la variable 1 es un literal numerico, este operao con una variable o con una variable ya declarada
        # o una que se encuentre en un instruccion de robot

        if (nombreVariable1== 'int'):
            try:
                tipoVariable2 = tablaSimb.diccionario[nombreVariable2]
            except:
                msg = "ERROR-EXP-BIN: La variable \'%s\' no esta declarada."
                e = msg % nombreVariable2
                print e
                sys.exit(1)

            if tipoVariable2 == 'int' or tipoVariable2 == 'collect' or tipoVariable2 == 'read':
                return 'int'
            
            if tipoVariable2 == 'bool':
                msg = "ERROR-EXP-BIN: La variable \'%s\' es bool y no se puede sumar."
                e = msg % nombreVariable2
                print e
                sys.exit(1)

        # Verificamos que si la variable 2 es un literal numerico, este operao con una variable o con una variable ya declarada
        # o una que se encuentre en un instruccion de robot

        if (nombreVariable2== 'int'):
            try:
                tipoVariable1 = tablaSimb.diccionario[nombreVariable1]
            except:
                msg = "ERROR-EXP-BIN: La variable \'%s\' no esta declarada."
                e = msg % nombreVariable1
                print e
                sys.exit(1)
            
            if tipoVariable1 == 'int' or tipoVariable1 == 'collect' or tipoVariable1 == 'read':
                return 'int'
            
            if tipoVariable1 == 'bool':
                msg = "ERROR-EXP-BIN: La variable \'%s\' es bool y no se puede sumar."
                e = msg % nombreVariable1
                print e
                sys.exit(1)


        # VERIFICACIÓN EXPRESION 1
        if (tablaSimb.diccionario.has_key(nombreVariable1) == True):
            tipoVariable1 = tablaSimb.diccionario[nombreVariable1]
        
        else:
            tablaPadre = tablaSimb.padre
            verifica = True         
            
            while (tablaPadre is not None) and verifica:
                
                if (tablaPadre.diccionario.has_key(nombreVariable1) == True):
                    tipoVariable1 = tablaPadre.diccionario[nombreVariable1]
                    verifica = False
                
                else:
                    tablaPadre = tablaPadre.padre
            
            if tablaPadre is None:
                msg = "ERROR-EXP-BIN: La variable \'%s\' no esta declarada."
                e = msg % nombreVariable1
                print e
                sys.exit(1)

        # VERIFICACIÓN EXPRESION 2
        if (tablaSimb.diccionario.has_key(nombreVariable2) == True):
            tipoVariable2 = tablaSimb.diccionario[nombreVariable2]
        
        else:
            tablaPadre = tablaSimb.padre
            verifica = True
            
            while (tablaPadre is not None) and verifica:
                
                if (tablaPadre.diccionario.has_key(nombreVariable2) == True):
                    tipoVariable2 = tablaPadre.diccionario[nombreVariable2]
                    verifica = False
                
                else:
                    tablaPadre = tablaPadre.padre
            
            if tablaPadre is None:
                msg = "ERROR-EXP-BIN: La variable \'%s\' no esta declarada."
                e = msg % nombreVariable2
                print e
                sys.exit(1)

        return self.chequear_tipos(tipoVariable1,tipoVariable2)

# END Expresion_Binaria.py
        