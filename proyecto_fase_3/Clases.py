#!/usr/bin/env python 
# -*- encoding: utf-8 -*-

################################################################################
#                      ANÁLISIS SINTÁCTICO - LENGUAJE BOT                      #
#                                                                              #
# Integrantes:                                                                 #
# 09-11133 Jorge Luis León                                                     #
# 10-10122 Maria Esther Carrillo                                               #
#                                                                              #
# Descripción:                                                                 #
# Módulo que contiene las clases con los procedimientos necesarios a ser       #
# utilizados por el Analizador Sintáctico (Parser.py) para construir el árbol  #
# sintáctico abstracto.                                                        #
#                                                                              #
#                                                                              #
################################################################################ 


import sys

count = -1 #Contador de alcance

#
# Funcion que se encarga de la espaciación adecuada en
# la impresión del árbol
#
def espaciacion(espacio):
    i = 0
    while (i < 4):
        espacio += " "
        i += 1
    return espacio

# END Clases.py




