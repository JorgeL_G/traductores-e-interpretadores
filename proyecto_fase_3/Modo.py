#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE MODO - LENGUAJE BOT                          #
#                                                                             #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendra como manejaremos la clase Booleana                    #
###############################################################################


class Modo:

    def __init__(self, valor):
        self.valor = valor
        self.type = "modo"

    def chequear(self, tablaSimb):
        return self.valor

# END Modo.py

