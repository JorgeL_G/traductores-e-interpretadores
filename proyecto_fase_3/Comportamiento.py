#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE COMPORTAMIENTO - LENGUAJE BOT                    #
#      			     on ⟨Condición⟩ : ⟨Instrucción de Robot⟩ end              #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase los comportamientos de robots   #
###############################################################################

import Tabla_Simbolos
import sys

class Comportamiento:

	def __init__(self,condicion,instruccion):	
		self.condicion = condicion
		self.instruccion = instruccion	

	def chequear(self, tablaSimb):
		tabNueva = Tabla_Simbolos.Tabla_Simbolos(tablaSimb)
		tablaSimb.enlazarHijo(tabNueva)
		tipo = self.condicion.chequear(tablaSimb)
		e = "Incompatibilidad de tipos para realizar el comportamiento"
		
		if tipo != "bool":

			if (tablaSimb.diccionario.has_key(tipo) == True):			
				e = "ERROR-DEC: El modo \'%s\' ya estaba declarado."
				print e % tipo
				sys.exit(1)
			
			else:
				tablaSimb.insertarElem(tipo,'modo')
		
		else:
			
			if tipo != "bool":
				print e % self.tipo
				sys.exit(1)

		if self.instruccion != None:
			for i in self.instruccion:
				i.chequear(tablaSimb)
		
		else:
			pass

# END Comportamiento.py
