#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                     CLASE PROGRAMA_PRINCIPAL - LENGUAJE BOT                 #       
# [create ⟨Lista de Declaraciones⟩] execute ⟨Instrucción de Controlador⟩ end  #                     
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de la estructura principal del  #
# programa.                                                                   #
###############################################################################

import Tabla_Simbolos
from Clases import *


class Programa_Principal:

    def __init__(self, declaraciones, instrucciones):
        self.declaraciones = declaraciones
        self.instrucciones = instrucciones

        tablaSimb = Tabla_Simbolos.Tabla_Simbolos(None)
        self.chequear(tablaSimb)

    def chequear(self, tablaSimb):

        if self.declaraciones != None:
            
            for i in self.declaraciones:
                i.chequear(tablaSimb)
        
        else:
            pass

        if self.instrucciones:
            self.instrucciones.chequear(tablaSimb)

# END Programa_Principal.py
