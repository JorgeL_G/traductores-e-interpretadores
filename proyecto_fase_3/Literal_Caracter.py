#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE LITERAL_CARACTER - LENGUAJE BOT                  #
#                                                                             #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de los tipos caracter. 	      #
###############################################################################


class Literal_Caracter:

    def __init__(self, valor):
        self.valor = valor
        self.type = "char"

    def chequear(self, tablaSimb):
        return self.valor

# END Literal_Caracter.py
