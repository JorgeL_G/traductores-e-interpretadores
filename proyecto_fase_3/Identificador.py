#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE IDENTIFICADOR - LENGUAJE BOT                     #
#                                                                             #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de los tipos identificadores.   #
###############################################################################

class Identificador:

    def __init__(self,nombre):      
        self.nombre = nombre
        self.type = "identificador"

    def chequear(self,tablaSimb):
		
		if self.nombre == 'me':
			msg = "ERROR-ID: La declaración 'me' no puede ser utilizada como identificador."
			print msg
			sys.exit(1)
		
		else:
			return self.nombre

# END Identificador.py
