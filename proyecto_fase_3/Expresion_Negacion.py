#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                     CLASE EXPRESION_BINARIA - LENGUAJE BOT                  #       
#                                  ~ ⟨Expresion⟩    						  #                     
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de las expresiones de tipo      #
# neagación.                                                                  #
###############################################################################


import sys

class Expresion_Negacion:

	def __init__(self,expresion):
		self.expresion = expresion
		self.type = "bool"

   	def chequear(self,tablaSimb):
   		nombreVariable = self.expresion.chequear(tablaSimb)

		if self.expresion.type == "exp_bin":
			tipoVariable = self.expresion.chequear(tablaSimb)
			
			if tipoVariable == "bool":
	   			return self.type
			
			else:
				e = "ERROR-NEGACION: Se esperaba que fuese "
				e += "de tipo \'bool\'." 
				print e
				sys.exit(1)
		
		else:
			
			if (tablaSimb.diccionario.has_key(nombreVariable) == True):
	   			tipoVariable = tablaSimb.diccionario[nombreVariable]
	   			
	   			if tipoVariable != "bool":
	   				e = "ERROR-NEGACION: Se esperaba que \'%s\'" % nombreVariable
					e += " fuese del tipo \'bool\'."				
					print e
					sys.exit(1)
	   			
	   			else:
					return self.type
	   		
	   		else:
	   			tablaPadre = tablaSimb.padre
				verifica = True
				
				while (tablaPadre is not None) and verifica:
					
					if (tablaPadre.diccionario.has_key(nombreVariable) == True):
						tipoVariable = tablaPadre.diccionario[nombreVariable]
			   			
			   			if tipoVariable != "bool":
			   				e = "ERROR-NEGACION: Se esperaba que \'%s\'" % nombreVariable
							e += " fuese del tipo \'bool\'."				
							print e
							sys.exit(1)
			   			
			   			else:
							return self.type
							verifica = False
					
					else:
						tablaPadre = tablaPadre.padre

				if tablaPadre is None:
					e = "ERROR-NEGACION: La variable \'%s\' no esta declarada."
					print e % nombreVariable
					sys.exit(1)

# END Expresion_Negacion.py
