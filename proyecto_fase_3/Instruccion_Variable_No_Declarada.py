#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#          CLASE INSTRUCCION_VARIABLE_NO_DECLARADA - LENGUAJE BOT             #       
#                           collect as ⟨Identi􏰹cador⟩                          #
#                           read as ⟨Identi􏰹cador⟩                             #                     
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de las intrucciones de robots   #
# cuyo identificador no debe haber sido previamente declarado.                #
###############################################################################

import sys

class Instruccion_Variable_No_Declarada:

    def __init__(self,tipo,identificador):  
        self.tipo = tipo
        self.identificador = identificador

    def chequear(self, tablaSimb):
        nombreVariable = self.identificador.chequear(tablaSimb)
        tipoVariable = self.tipo

        if (tablaSimb.diccionario.has_key(nombreVariable) == True):         
            e = "ERROR-DEC: La variable \'%s\' ya esta declarada."
            print e % nombreVariable
            sys.exit(1)
            
        tablaSimb.insertarElem(nombreVariable, tipoVariable)



# END Instruccion_Variable_No_Declarada.py



