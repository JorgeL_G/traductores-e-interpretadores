#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE TIPO - LENGUAJE BOT                              #
#                                                                             #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendra como manejaremos la clase Tipo                        #
###############################################################################


class Tipo:

    def __init__(self, valor):
        self.valor = valor

    def imprimir(self, espacio, tablaSimb):
        pass

    def chequear(self, tablaSimb):
        return self.valor

# END Tipo.py
