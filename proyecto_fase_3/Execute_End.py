#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                       CLASE EXECUTE_END - LENGUAJE BOT                      #       
#                    execute ⟨Instrucción de Controlador⟩ end                 #                     
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de las intrucciones del         #
# controlador en el cuerpo del mismo.                                         #
###############################################################################

import Tabla_Simbolos


class Execute_End:

    def __init__(self, instrucciones):
        self.instrucciones = instrucciones
        tablaSimb = Tabla_Simbolos.Tabla_Simbolos(None)
        self.chequear(tablaSimb)

    def chequear(self, tablaSimb):
        
        if self.instrucciones != [None]:
            self.instrucciones.chequear(tablaSimb)
            
# END Execute_End.py
