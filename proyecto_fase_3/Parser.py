#!/usr/bin/env python
# -*- encoding: utf-8 -*-

################################################################################
#                      ANÁLISIS SINTÁCTICO - LENGUAJE BOT                      #
#                                                                              #
# Integrantes:                                                                 #
# 09-11133 Jorge Luis León                                                     #
# 10-10122 Maria Esther Carrillo                                               #
#                                                                              #
# Descripción:                                                                 #
# Módulo que contiene la definición de las gramáticas para la construcción del # 
# árbol sintáctico abstracto.                                                  #
#                                                                              #
################################################################################ 


import sys

import ply.yacc as yacc
from LexBot import LexBot
from Clases import *

import Programa_Principal
import Asignacion
import Booleano
import Ciclo_While
import Comportamiento
import Condicional_If
import Lista_Declaraciones
import Expresion_Binaria
import Expresion_Negacion
import Expresion_Negativo
import Identificador
import Instruccion_Variable_No_Declarada
import Literal_Caracter
import Literal_Numerico
import Modo
import Tipo
import Tabla_Simbolos

archivo = ""
i = 0

########### ESTRUCUTRA PRINCIPAL DEL PROGRAMA ###########

#
# Función para la estructura principal del programa
#
def p_inicio_programa(p):
    '''PROGRAMA : TkCreate LISTA_DECLARACIONES TkExecute LISTA_INSTRUCCIONES_CONTROLADOR TkEnd
                | TkExecute LISTA_INSTRUCCIONES_CONTROLADOR TkEnd'''

    if len(p) == 4:
        p[0] = Programa_Principal.Programa_Principal(None, p[2])
    else:
        p[0] = Programa_Principal.Programa_Principal(p[2], p[4])


########### LISTAS DE DECLARACIONES DE ROBOTS ###########

#
# Función para las listas de declaraciones de diferentes robots
#
def p_lista_declaraciones(p):
    '''LISTA_DECLARACIONES : LISTA_DECLARACIONES TIPO TkBot LISTA_IDENTIFICADORES LISTA_COMPORTAMIENTOS TkEnd 
                           | LISTA_DECLARACIONES TIPO TkBot LISTA_IDENTIFICADORES TkEnd'''

    p[0] = p[1]
    if len(p) == 7: 
        p[0].append(Lista_Declaraciones.Lista_Declaraciones(p[2],p[4],p[5]))
    else:
        p[0].append(Lista_Declaraciones.Lista_Declaraciones(p[2],p[4],None))


#
# Función auxiliar para el caso base de las listas de declaraciones de diferentes robots
#
def p_lista_declaraciones_base(p):
    '''LISTA_DECLARACIONES : TIPO TkBot LISTA_IDENTIFICADORES LISTA_COMPORTAMIENTOS TkEnd
                           | TIPO TkBot LISTA_IDENTIFICADORES TkEnd'''

    p[0] = []
    if len(p) == 6:
        p[0].append(Lista_Declaraciones.Lista_Declaraciones(p[1],p[3],p[4]))
    else:
        p[0].append(Lista_Declaraciones.Lista_Declaraciones(p[1],p[3],None))


#
# Función para los tipos asociados a los robots
#
def p_tipo(p):
    '''TIPO : TkInt
            | TkBool
            | TkChar'''

    p[0] = p[1]

#
# Función para identificadores de robots
#
def p_lista_id(p):
    '''LISTA_IDENTIFICADORES : LISTA_IDENTIFICADORES TkIdent TkComa 
                             | LISTA_IDENTIFICADORES TkIdent'''

    if len(p) == 2:
        Identificador.Identificador(p[1])
    else:
        p[0] = p[1]

#
# Función auxiliar para el caso base de identificadores de robots
#
def p_lista_id_base(p):
    '''LISTA_IDENTIFICADORES : TkIdent TkComa 
                             | TkIdent'''
    p[0] = Identificador.Identificador(p[1])

#
# Función para la listas de comportamientos de robots
#
def p_lista_comportamientos(p):
    '''LISTA_COMPORTAMIENTOS : LISTA_COMPORTAMIENTOS TkOn MODO TkDosPuntos LISTA_INSTRUCCIONES TkEnd 
                             | LISTA_COMPORTAMIENTOS TkOn EXPRESION TkDosPuntos LISTA_INSTRUCCIONES TkEnd'''
    
    p[0] = p[1]
    p[0].append(Comportamiento.Comportamiento(p[3],p[5]))

#
# Función auxiliar para el caso base de las listas de comportamientos de robots
#
def p_lista_comportamientos_base(p):
    '''LISTA_COMPORTAMIENTOS : TkOn MODO TkDosPuntos LISTA_INSTRUCCIONES TkEnd 
                             | TkOn EXPRESION TkDosPuntos LISTA_INSTRUCCIONES TkEnd'''
    
    p[0] = []
    p[0].append(Comportamiento.Comportamiento(p[2],p[4]))

#
# Función para parte de las condiciones de un robot
#
def p_modo(p):
    ''' MODO : TkActivation
             | TkDeactivation
             | TkDefault'''

    p[0] = Modo.Modo(p[1])


########### LISTAS DE INSTRUCCIONES DE ROBOTS ###########

#
# Función para las listas de instrucciones asociadas a un comportamiento
#
def p_lista_instrucciones(p):
    '''LISTA_INSTRUCCIONES : LISTA_INSTRUCCIONES INSTRUCCIONES ''' 
                         

    p[0] = p[1]
    p[0].append(p[2])

#
# Función auxiliar para el caso base de las listas de instrucciones asociadas a un comportamiento
#
def p_lista_instrucciones_base(p):
    '''LISTA_INSTRUCCIONES : INSTRUCCIONES ''' 
                         
    p[0] = []
    p[0].append(p[1])


#
# Función de las posibles instrucciones de un robot
#
def p_instrucciones(p):
    '''INSTRUCCIONES : RECEIVE
                     | STORE
                     | COLLECT 
                     | DROP
                     | MOVIMIENTO
                     | E_S'''

    p[0] = p[1]

#
# Función para la instrucción 'receive'
#
def p_receive(p):
    '''RECEIVE : TkReceive TkPunto'''

#
# Función para la instrucción 'store'
#
def p_store(p):
    '''STORE : TkStore TkMe EXPRESION TkPunto
             | TkStore EXPRESION TkPunto'''
    
    if len(p) == 5:
        p[0] = p[3]
    else:
        p[0] = p[2]

#
# Función para la instrucción 'drop'
#
def p_drop(p):
    '''DROP : TkDrop TkMe EXPRESION TkPunto
            | TkDrop EXPRESION TkPunto'''
    
    if len(p) == 5:
        p[0] = p[3]
    else:
        p[0] = p[2]

#
# Función para la instrucción 'collect'
#
def p_collect(p):
    '''COLLECT : TkCollect TkAs IDENTIFICADOR TkPunto
               | TkCollect TkPunto'''

    if len(p) == 5:
        p[0] = Instruccion_Variable_No_Declarada.Instruccion_Variable_No_Declarada('collect', p[3])

#
# Función para las instrucciones de entrada y salida
#
def p_entrada_salida(p):
    '''E_S : TkRead TkAs IDENTIFICADOR TkPunto
           | TkRead TkPunto
           | TkSend TkPunto'''
    
    if (p[1] == 'read'):
        if len(p) == 5:
            p[0] = Instruccion_Variable_No_Declarada.Instruccion_Variable_No_Declarada('read', p[3])

#
# Función para las posibles instrucciones de movimiento
#
def p_movimiento(p):
    '''MOVIMIENTO : TkUp EXPRESION TkPunto
                  | TkUp TkPunto
                  | TkDown EXPRESION TkPunto
                  | TkDown TkPunto
                  | TkLeft EXPRESION TkPunto
                  | TkLeft TkPunto
                  | TkRight EXPRESION TkPunto
                  | TkRight TkPunto'''
    
    if len(p) == 4:
        p[0] = p[2]


########### LISTAS DE INSTRUCCIONES DEL CONTROLADOR ###########
        
#
# Función para la lista de instrucciones del controlador
#
def p_lista_instrucciones_controlador(p):
    '''LISTA_INSTRUCCIONES_CONTROLADOR : INSTRUCCIONES_CONTROLADOR LISTA_INSTRUCCIONES_CONTROLADOR
                                       | INSTRUCCIONES_CONTROLADOR'''
  
    p[0] = p[1]


#
# Función para las posibles instrucciones del controlador
#
def p_instrucciones_controlador(p):
    '''INSTRUCCIONES_CONTROLADOR : ACTIVATE
                                 | ADVANCE
                                 | DEACTIVATE
                                 | IF
                                 | WHILE
                                 | PROGRAMA'''
  
    p[0] = p[1]

#
# Función para la instrucción 'activate'
#
def p_activate (p):
    '''ACTIVATE : TkActivate LISTA_IDENTIFICADORES TkPunto'''
    p[0] = p[2]

#
# Función para la instrucción 'deactivate'
#
def p_deactivate (p):
    '''DEACTIVATE : TkDeactivate LISTA_IDENTIFICADORES TkPunto'''

    p[0] = p[2]

#
# Función para la instrucción 'advance'
#
def p_advance (p):
    '''ADVANCE : TkAdvance LISTA_IDENTIFICADORES TkPunto'''

    p[0] = p[2]

#
# Función para la instrucción condicional
#
def p_if_else (p):
    '''IF : TkIf EXPRESION TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkElse TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkEnd
          | TkIf EXPRESION TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkEnd'''

    if p[5] == 'else':
        p[0] = Condicional_If.Condicional_If(p[2],p[4],p[7])
    else : 
        p[0] = Condicional_If.Condicional_If(p[2],p[4],None)

#
# Función para la instrucción de iteración indeterminada
#
def p_while (p):
    '''WHILE : TkWhile EXPRESION TkDosPuntos LISTA_INSTRUCCIONES_CONTROLADOR TkEnd'''

    p[0] = Ciclo_While.Ciclo_While(p[2],p[4])


########### EXPRESIONES ###########

# Lista de precedencia de los operandos
precedencia = (
    ('left','TkMayor','TkMayorIgual','TkMenor','TkMenorIgual','TkIgual','TkDistinto'),
    ('left','TkSuma','TkResta'),
    ('left','TkDiv','TkMult','TkMod'),
    ('left','TkDisyuncion','TkConjuncion'),
    ('left','TkNegacion','TkMenos'), 
)

#
# Función para los posibles tipos de instrucciones
#
def p_valor_expresion(p):
    '''EXPRESION : BOOL
                 | NUM
                 | CARACTER
                 | IDENTIFICADOR
                 | TkMe'''
    p[0] = p[1]

#
# Función para las expresiones de tipo booleano
#
def p_boolo(p):
    '''BOOL : TkTrue
            | TkFalse'''

    p[0] = Booleano.Booleano(p[1])

#
# Función para las expresiones de tipo literal numérico
#
def p_literal_numerico(p):
    '''NUM : TkNum'''

    p[0] = Literal_Numerico.Literal_Numerico(p[1])

#
# Función para las expresiones de tipo literal caracter
#
def p_literal_caracter(p):
    '''CARACTER : TkCaracter'''

    p[0] = Literal_Caracter.Literal_Caracter(p[1])

#
# Función para las expresiones de tipo identificador
#
def p_identificador(p):
    '''IDENTIFICADOR : TkIdent'''

    p[0] = Identificador.Identificador(p[1])

#
# Función para las expresiones unarias
#
def p_expresion_unaria(p):
    '''EXPRESION  : TkNegacion EXPRESION
                  | TkResta    EXPRESION'''

    if p[1] == '-':
        p[0] = Expresion_Negativo.Expresion_Negativo(p[2])
    else:
        p[0] = Expresion_Negacion.Expresion_Negacion(p[2])

#
# Función para las instrucciones binarias
#
def p_expresion_binaria(p):
    '''EXPRESION :   EXPRESION TkSuma         EXPRESION
                   | EXPRESION TkResta        EXPRESION
                   | EXPRESION TkMul          EXPRESION
                   | EXPRESION TkDiv          EXPRESION
                   | EXPRESION TkMod          EXPRESION
                   | EXPRESION TkConjuncion   EXPRESION 
                   | EXPRESION TkDisyuncion   EXPRESION
                   | EXPRESION TkMayor        EXPRESION
                   | EXPRESION TkMayorIgual   EXPRESION
                   | EXPRESION TkMenor        EXPRESION
                   | EXPRESION TkMenorIgual   EXPRESION
                   | EXPRESION TkIgual        EXPRESION           
                   | EXPRESION TkDistinto     EXPRESION'''

    p[0] = Expresion_Binaria.Expresion_Binaria(p[1], p[2], p[3])

#
# Función para las expresiones parentizadas
#
def p_expresion_parentesis(p):
    ''' EXPRESION : TkParAbre EXPRESION TkParCierra'''

    p[0] = p[2]


########### MANEJO DE ERRORES SINTÁCTICOS ###########

#
# Función para el manejo de errores de sintaxis
#
def p_error(p):
    if p is not None:
        # A pesar de encontrar el error, no imprime correctamente la línea del error sintáctico 
        print "ERROR DE SINTAXIS:"+  " Token Inesperado '" + str(p.value) + "' (Linea: " + str(p.lineno) + ")"  
        exit()    
    else:
        print "Error de sintaxis: falta de token lo que impide cumplimiento de gramatica"
        exit()


########### ANÁLISIS SINTÁCTICO ###########

#
# Función que simula el analizador sintáctico del lenguaje BOT
#
def analizador_contexto(archivo_texto):

    try:
        tokens = LexBot.tokens
        espacio = " "

        lexer_bot = LexBot()
        lexer_bot.build()
        lexer_bot.analizador_lexicografico(archivo_texto)

        # Apertura del archivo de entrada
        f = open(archivo_texto)

        # Lectura del archivo de entrada
        data = f.read()

        # Construcción del parser
        parser = yacc.yacc()
        result = parser.parse(data)

        # Cierre del archivo de texto
        f.close()

    except IOError:

        print 'ERROR: No se pudo abrir el archivo de texto \"%s\"' % archivo_texto
        exit()

# END Parser.py


