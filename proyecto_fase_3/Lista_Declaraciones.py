#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE LISTA_DECLARACIONES - LENGUAJE BOT               #       
#    ⟨Tipo⟩ bot ⟨Lista de Identi􏰹cadores⟩ [ ⟨Lista de Comportamientos⟩ ] end   # 
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de las listas de declaraciones  #
# de los diferentes definiciones de robots.                                   #
###############################################################################

import sys

class Lista_Declaraciones:

    def __init__(self,tipo,identificador,comportamiento):  
        self.tipo = tipo
        self.identificador = identificador
        self.comportamiento = comportamiento

    def chequear(self, tablaSimb):
        nombreVariable = self.identificador.chequear(tablaSimb)
        tipoVariable = self.tipo

        if (tablaSimb.diccionario.has_key(nombreVariable) == True):         
            e = "ERROR-DEC: La variable \'%s\' ya esta declarada."
            print e % nombreVariable
            sys.exit(1)    
        
        tablaSimb.insertarElem(nombreVariable, tipoVariable)

        if self.comportamiento != None:
            for i in self.comportamiento:
                i.chequear(tablaSimb)


# END Lista_Declaraciones.py
