#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE TABLA_DE_SIMBOLOS - LENGUAJE BOT                 #
#                                                                             #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Clase que contendra la tabla de símbolos que se utilizará en la             #
# verificación de tipos.                                                      #
###############################################################################

class Tabla_Simbolos:

    def __init__(self, padre):
        self.diccionario = {}
        self.hijos = []
        self.padre = padre
        self.nombre = ""

    def setNombre(self, nombre):
        self.nombre = nombre

    def getNombre(self):
        return self.nombre

    def insertarElem(self, nombreVar, tipo):
        self.diccionario[nombreVar] = tipo

    def enlazarHijo(self, hijo):
        self.hijos.append(hijo)

# END Tabla_Simbolos.py
