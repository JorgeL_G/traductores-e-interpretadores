#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE CICLO_WHILE - LENGUAJE BOT                       #
#      					  while ⟨Bool⟩ : ⟨Instr⟩ end                          #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase las intrucciones de iteración   #
# indeterminadas														      #
###############################################################################

import sys

class Ciclo_While:

	def __init__(self,condicion,instruccion):
		self.condicion = condicion
		self.instruccion = instruccion

	def chequear(self,tablaSimb):
		f = "ERROR-WHILE: Se esperaba que la condicion fuese de tipo \'bool\'."
		if self.condicion.type == "identificador":
			nombreCond = self.condicion.chequear(tablaSimb)

			if (tablaSimb.diccionario.has_key(nombreCond) == True):
				tipoCond = tablaSimb.diccionario[nombreCond]
				
				if tipoCond != "bool":
					print f
					sys.exit(1)
			
			else:
				tablaPadre = tablaSimb.padre
				while (tablaPadre != None):
					
					if (tablaPadre.diccionario.has_key(nombreCond) == True):
						tipoCond = tablaPadre.diccionario[nombreCond]
						
						if (tipoCond != "bool"):
							e = f
							print e
							sys.exit(1)
						
						else: 
							break
					
					else:
						tablaPadre = tablaPadre.padre

				if tablaPadre == None:
					e = "ERROR-WHILE: La variable \'%s\' no esta declarada." 
					print e % nombreCond
					sys.exit(1)

		elif self.condicion.type == "exp_bin":
			tipoExp = self.condicion.chequear(tablaSimb)
			
			if tipoExp != "bool":
				print f
				sys.exit(1)
		else:
			tipoExp = self.condicion.chequear(tablaSimb)
			
			if tipoExp != "bool":
				print f
				sys.exit(1)

		if self.instruccion:
			self.instruccion.chequear(tablaSimb)

# END Ciclo_While.py
