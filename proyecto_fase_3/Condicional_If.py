#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                      CLASE CICLO_IF - LENGUAJE BOT                          #                                          #
#                if ⟨Bool⟩ : ⟨Instr1⟩ [ else : ⟨Instr2⟩ ] end                 #
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase las intrucciones condicionales  #                                                   #
###############################################################################

import sys

class Condicional_If:

    def __init__(self,condicion,instruccion1,instruccion2=None):
        self.condicion = condicion
        self.instruccion1 = instruccion1
        self.instruccion2 = instruccion2

    def chequear(self,tablaSimb):
        f = "ERROR-IF: Se esperaba que la condicion fuese de tipo \'bool\'."
        
        if self.condicion.type == "identificador":
            nombreCond = self.condicion.chequear(tablaSimb)
            
            if (tablaSimb.diccionario.has_key(nombreCond) == True):
                tipoCond = tablaSimb.diccionario[nombreCond]
                
                if tipoCond != "bool":
                    print f
                    sys.exit(1)
            else:
                tablaPadre = tablaSimb.padre
                
                while (tablaPadre is not None):
                    
                    if (tablaPadre.diccionario.has_key(nombreCond) == True):
                        tipoCond = tablaPadre.diccionario[nombreCond]
                        
                        if (tipoCond != "bool"):
                            print f
                            sys.exit(1)
                        
                        else: 
                            break
                    
                    else:
                        tablaPadre = tablaPadre.padre

                if tablaPadre == None:
                    e = "ERROR-IF: La variable \'%s\' no esta declarada." 
                    print e % nombreCond
                    sys.exit(1)
                    
        elif self.condicion.type == "exp_bin":
            tipoExp = self.condicion.chequear(tablaSimb)

            if tipoExp != "bool":
                print f
                sys.exit(1)
        
        else:
            tipoExp = self.condicion.chequear(tablaSimb)
            if tipoExp != "bool":
                print f
                sys.exit(1)

        if self.instruccion1:
            self.instruccion1.chequear(tablaSimb)

        if self.instruccion2:
            self.instruccion2.chequear(tablaSimb)

# END Condicional_If.py
