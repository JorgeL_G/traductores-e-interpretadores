#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                     CLASE EXPRESION_BINARIA - LENGUAJE BOT                  #       
#                                  - ⟨Expresion⟩    						  #                     
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de las expresiones de tipo      #
# negativo.                                                                   #
###############################################################################

import sys

class Expresion_Negativo:

	def __init__(self,expresion):
		self.expresion = expresion
		self.type = ""

	def setType(self,tipo):
		self.type = tipo;

   	def chequear(self,tablaSimb):
   		nombreVariable = self.expresion.chequear(tablaSimb)
	 
   		if isinstance(self.expresion,Expresion_Negativo):
			self.expresion.chequear(tablaSimb)
			return self.type

		if nombreVariable == 'int':
			return 'int'

		if (tablaSimb.diccionario.has_key(nombreVariable) == True):
   			tipoVariable = tablaSimb.diccionario[nombreVariable]

   			if tipoVariable == "bool":
   				e = "ERROR-NEG: Incompatibilidad con la negatividad"				
				print e
				sys.exit(1)
   			
   			else:
   				self.setType(tipoVariable)
				return tipoVariable	
   		
   		else:
   			tablaPadre = tablaSimb.padre
			verifica = True
			
			while (tablaPadre is not None) and verifica:
				
				if (tablaPadre.diccionario.has_key(nombreVariable) == True):
					tipoVariable = tablaPadre.diccionario[nombreVariable]
					
					if tipoVariable == "bool":	
						print "ERROR-NEG: Incompatibilidad con la negatividad."
						sys.exit(1)
					
					else:
						verifica = False
						self.setType(tipoVariable)
						return tipoVariable					
				
				else:
					tablaPadre = tablaPadre.padre

			if tablaPadre is None:
				e = "ERROR-NEG: La variable \'%s\' no esta declarada."
				print e % nombreVariable
				sys.exit(1)
					
# END Expresion_Negativo.py
		