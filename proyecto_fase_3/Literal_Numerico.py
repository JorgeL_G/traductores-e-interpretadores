#!/usr/bin/env python
# -*- encoding: utf-8 -*-
###############################################################################
#                     CLASE EXPRESION_BINARIA - LENGUAJE BOT                  #       
#													 						  #                     
# Integrantes:                                                                #
# 09-11133 Jorge Luis León                                                    #
# 10-10122 Maria Esther Carrillo                                              #
#                                                                             #
# Descripción:                                                                #
# Archivo que contendrá el manejo de la clase de los tipos numérico.          #
###############################################################################

class Literal_Numerico:

    def __init__(self, valor):
        self.valor = valor
        self.type = "int"

    def retornaValor(self):
        return self.valor

    def chequear(self, tablaSimb):
        return self.type

# END Literal_Numerico.py
